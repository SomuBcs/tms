﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using System.Web.Security;
using TMS.EFModel;
using TMS.Models;
using TMS.BusinessEntities;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace TMS.Controllers
{
    public class LoginController : Controller
    {
        public void dispayUserInfo()
        {
            HttpCookie AuthCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(AuthCookie.Value);

            DataTable UserData = LoginModel.getUserProfile(ticket.Name);

            UserInfo.UserID = Convert.ToInt32(UserData.Rows[0]["UserID"]);
            UserInfo.UserName = UserData.Rows[0]["UserName"].ToString();
            UserInfo.BranchID = Convert.ToInt32(UserData.Rows[0]["BranchID"]);
            UserInfo.ZoneID = Convert.ToInt32(UserData.Rows[0]["ZoneID"]);
            UserInfo.RoleID = Convert.ToInt32(UserData.Rows[0]["RoleID"]);
            //UserInfo.ReportingManagerID = Convert.ToInt32(UserData.Rows[0]["ReportingManagerID"]);

            ViewBag.UserID = UserData.Rows[0]["UserID"].ToString();
            ViewBag.UserName = UserData.Rows[0]["UserName"].ToString();
            ViewBag.BranchID = UserData.Rows[0]["BranchID"].ToString();
            ViewBag.ZoneID = UserData.Rows[0]["ZoneID"].ToString();
            ViewBag.RoleID = Convert.ToInt32(UserData.Rows[0]["RoleID"].ToString());
            // ViewBag.USERROLES = UserData.Rows[0]["USERROLES"].ToString();
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }
        [HttpPost]
        public ActionResult LoadHome()
        {
            // dispayUserInfo();
            return Json(Url.Action("Home", "Login"));
        }


        public ActionResult Home()
        {
            //dispayUserInfo();
            return View();
        }


        public JsonResult ValidateUser(string emailID, String userPword)
        {
            bool isValid = false;
            try
            {
                UserMaster loginUser = LoginModel.ValidateUser(emailID, userPword);
                if (loginUser != null)
                {
                    UserInfo.UserID = loginUser.UserID;

                    UserInfo.UserName = loginUser.UserName;
                    UserInfo.BranchID = loginUser.BranchID;
                    UserInfo.ZoneID = loginUser.ZoneID;
                    UserInfo.RoleID = loginUser.RoleID;
                    if (loginUser.ReportingManagerID != null)
                        UserInfo.ReportingManagerID = (int)loginUser.ReportingManagerID;

                    ViewBag.UserID = loginUser.UserID;

                    ViewBag.UserName = loginUser.UserName; ;
                    ViewBag.BranchID = loginUser.BranchID;
                    ViewBag.ZoneID = loginUser.ZoneID;
                    ViewBag.RoleID = loginUser.RoleID;

                    FormsAuthentication.SetAuthCookie(UserInfo.EmailID, true);
                    isValid = true;
                }
                
                return Json(isValid,JsonRequestBehavior.AllowGet );
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}