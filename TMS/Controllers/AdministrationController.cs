﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TMS.Controllers
{
    public class AdministrationController : Controller
    {
        // GET: Administration
        public ActionResult UserMaster()
        {
            return View();
        }

        public ActionResult UserRoles()
        {
            return View();
        }

        public ActionResult Vehicles()
        {
            return View();
        }
    }
}