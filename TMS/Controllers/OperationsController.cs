﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using TMS.EFModel;
using TMS.Models;
using TMS.BusinessEntities;
namespace TMS.Controllers
{
    public class OperationsController : Controller
    {
        string CONS_TransactionType = "Consignment";
        string MANIFEST_TransactionType = "Manifest";
        #region Views
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Consignment()
        {
            ViewData.Add("BranchId", UserInfo.BranchID);
            return View();
        }
        public ActionResult VehicleHire()
        {
            ViewData.Add("BranchId", UserInfo.BranchID);
            return View();
        }
        public ActionResult LocalVehicleHire()
        {
            return View();
        }
        public ActionResult ManifestUnload()
        {
            return View();
        }
        public ActionResult ConsignmentDeliveryStatus()
        {
            return View();
        }

        public ActionResult ManifestReport()
        {
            return View();
        }
        #endregion
        #region Consignment Booking
        public void UpdateConsignmentBooking(ConsignmentBooking cnsgnBooking)
        {
            try
            {
                int loginID = UserInfo.UserID;
              //  string loginID = UserInfo.EMPLOYEEID;
                    ConsignmentModel.UpdateConsignmentBooking(loginID, cnsgnBooking);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int  AddConsignmentBooking(ConsignmentBooking cnsgnBooking)
        {
            try
            {
                int loginID = UserInfo.UserID;
                cnsgnBooking.BranchID = UserInfo.BranchID;
                cnsgnBooking.ZoneID= UserInfo.ZoneID;
               int consignmentID =  ConsignmentModel.AddConsignmentBooking(loginID, cnsgnBooking);
                return consignmentID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteConsignmentBooking(int consignID )
        {
            try
            {
                int loginID = UserInfo.UserID;
                ConsignmentModel.DeleteConsignmentBooking(loginID, consignID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;

            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }


        public string GetLocalVehiclesHired()
        {
            try
            {
                int BranchID = UserInfo.BranchID;
                string vehicleJSON = DataTableToJSON(ConsignmentModel.GetLocalVehiclesHired(BranchID));
                return vehicleJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string  GetConsignmentBookings()
        {
            try
            {
                int branchID = UserInfo.BranchID;
                string bookingJSON = DataTableToJSON(ConsignmentModel.GetConsignmentBookings(branchID));
             //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);
               
                return bookingJSON;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetConsignmentBookingByID(int consignmentID)
        {
            try
            {
               
                var bookings = Json(ConsignmentModel.GetConsignmentBookingByID(consignmentID), JsonRequestBehavior.AllowGet);
                return bookings;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Consignment Booking payment
        public double GetTotalFreight (int consignmentID)
        {
            try
            {
                return ConsignmentModel.GetTotalBasicFreight(consignmentID);
            }
            catch(Exception ex )
            { throw ex; }
        }

        public double GetInvoiceTotal(int consignmentID)
        {
            try
            {
                return ConsignmentModel.GetInvoiceTotal(consignmentID);
            }
            catch (Exception ex)
            { throw ex; }
        }
        public void UpdateConsignmentPayment(ConsignmentBookingPayment bookingPayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                //  string loginID = UserInfo.EMPLOYEEID;
                ConsignmentModel.UpdateConsignmentPayment(loginID, bookingPayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddConsignmentPayment(ConsignmentBookingPayment bookingPayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
               
                ConsignmentModel.AddConsignmentPayment(loginID, bookingPayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteConsignmentBookingPayment(int paymenID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ConsignmentModel.DeleteConsignmentBookingPayment(loginID, paymenID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

        public JsonResult GetConsignmentBookingPaymentByID(int consignmentID)
        {
            try
            {

                var payment = Json(ConsignmentModel.GetConsignmentBookingPaymentByID(consignmentID), JsonRequestBehavior.AllowGet);
                return payment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Consignment Party Invoice
       [HttpPost]
        public void AddConsignmentPartyInvoice(ConsignmentPartyInvoice newpartyInvoice)
        {
            try
            {
                int loginID = UserInfo.UserID;
               
                ConsignmentModel.AddConsignmentPartyInvoice(loginID, newpartyInvoice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteConsignmentPartyInvoice(int partyInvoiceID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ConsignmentModel.DeleteConsignmentPartyInvoice(loginID, partyInvoiceID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
       
        public string  GetConsignmentPartyInvoice(int consignmentID)
        {
            try
            {

                string invoices = DataTableToJSON(ConsignmentModel.GetConsignmentPartyInvoices(consignmentID));
                return invoices;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
       
        #region VEHICLE MASTER
        public void AddVehicle(VehicleMaster vehicle)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ManifestModel.AddVehicle(loginID, vehicle);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region MASTER DATA RETREIVAL
        public JsonResult GetVehicleByID(int vehicleID)
        {
            try
            {
                var obj = ConsignmentModel.GetVehicleByID(vehicleID);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            { throw ex; }
        }

        public JsonResult GetVehicleHirePaymentTypes()
        {
            try
            {
                var obj = ManifestModel.GetVehicleHirePaymentTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetTruckBookingTypes()
        {
            try
            {
                var obj = ManifestModel.GetTruckBookingTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDeliveryStatusMaster()
        {
            try
            {
                var obj = ManifestModel.GetDeliveryStatusMaster();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
               
                throw x;
            }
        }
        public JsonResult GetVendorTypes()
        {
            try
            {
                var obj = ManifestModel.GetVendorTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetBranches()
        {
            try
            {
                var obj = ConsignmentModel.GetBranches();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetCargoTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetCargoTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetVehicles(bool isHired)
        {
            try
            {
                var obj = ConsignmentModel.GetVehicles(isHired);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetCompanies()
        {
            try
            {
                var obj = ConsignmentModel.GetCompanies();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDeliveryTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetDeliveryTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetChargeTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetChargeTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDoorCollections()
        {
            try
            {
                var obj = ConsignmentModel.GetDoorCollections();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetPackagings()
        {
            try
            {
                var obj = ConsignmentModel.GetPackagings();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        public JsonResult GetPaymentTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetPaymentTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetGoods()
        {
            try
            {
                var obj = ConsignmentModel.GetGoods();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetTaxes()
        {
            try
            {
                var obj = ConsignmentModel.GetTaxes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetServiceTaxpaidBy()
        {
            try
            {
                var obj = ConsignmentModel.GetServiceTaxpaidBy();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetTransportModes()
        {
            try
            {
                var obj = ConsignmentModel.GetTransportModes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        public JsonResult GetTransportTypes()
        {
            try
            {

                var obj = ConsignmentModel.GetTransportTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public string GetNextConsignmentNo(int branchID)
        {
            try
            {
                string consignNo = ConsignmentModel.GetNextRunningNo(branchID,CONS_TransactionType);
                return consignNo;
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public string GetNextManifestNo(int branchID)
        {
            try
            {
                string consignNo = ConsignmentModel.GetNextRunningNo(branchID, MANIFEST_TransactionType);
                return consignNo;
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetZones()
        {
            try
            {
                var obj = ConsignmentModel.GetZones();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        public JsonResult GetConsignmentReceivedStatus()
        {
            try
            {

                var obj = ManifestModel.GetConsignmentReceivedStatus();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        #endregion

        #region VEHICLE HIRE PAYMENT
        public void UpdateVehicleHirePayment(VehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                //  string loginID = UserInfo.EMPLOYEEID;
                ManifestModel.UpdateVehicleHirePayment(loginID, hirepayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public void AddVehicleHirePayment(VehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                hirepayment.BranchID = UserInfo.BranchID;
                ManifestModel.AddVehicleHirePayment(loginID, hirepayment);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteVehicleHire(int  vehicleHireID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ManifestModel.DeleteVehicleHire(loginID, vehicleHireID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string  GetVehicleHirePayments()
        {
            try
            {
                int branchID = UserInfo.BranchID;
                return DataTableToJSON(ManifestModel.GetVehicleHirePayments(branchID));
                //return  Json(ManifestModel.GetVehicleHirePayments(branchID), JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetVehicleHirePaymentByID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(ManifestModel.GetVehicleHirePaymentByID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region VEHICLE MANIFEST
        public string GetConsignmentsForManifest(int manifestId)
        {
            try
            {
                int branchID = UserInfo.BranchID;
                string bookingJSON = DataTableToJSON(ManifestModel.GetConsignmentsForManifest(branchID, manifestId));
                //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        [HttpPost]
        public void UpdateVehicleHireManifest(VehicleHireManifest manifest)
        {
            try
            {
                //VehicleHireManifest vhm = JsonConvert.DeserializeObject<VehicleHireManifest>(manifest);
                int loginID = UserInfo.UserID;
               // manifest.EstimatedDeliveryDate = System.DateTime.Now;//TEMPORARY ADDED
                ManifestModel.UpdateVehicleHireManifest(loginID, manifest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int  AddVehicleHireManifest(VehicleHireManifest manifest)
        {
            try
            {
                int loginID = UserInfo.UserID;
                int branchID = UserInfo.BranchID;
               manifest.EstimatedDeliveryDate = System.DateTime.Now;//TEMPORARY ADDED
                int manifestID = ManifestModel.AddVehicleHireManifest(loginID, branchID, manifest);
                return manifestID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteVehicleHireManifest(int manifestID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ManifestModel.DeleteVehicleHireManifest(loginID, manifestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetVehicleHireManifestByManifestID(int manifestID)
        {
            try
            {
                
                return Json(ManifestModel.GetVehicleHireManifestByManifestID(manifestID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(ManifestModel.GetVehicleHireManifestByVehicleHireID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Manifest consignments
        public void AddManifestConsignment(List<ManifestConsignmentMapping> manifestConsignments)
        {
            try
            {
                int loginID = UserInfo.UserID;

                ManifestModel.AddManifestConsignment(loginID, manifestConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetManifestConsignments(int manifestID)
        {
            try
            {

                var manifestConsignments = Json(ManifestModel.GetManifestConsignments(manifestID), JsonRequestBehavior.AllowGet);
                return manifestConsignments;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region LOCAL VEHICLE HIRE PAYMENT
        public void UpdateLocalVehicleHirePayment(LocalVehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                //  string loginID = UserInfo.EMPLOYEEID;
                LocalManifestModel.UpdateLocalVehicleHirePayment(loginID, hirepayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void AddLocalVehicleHirePayment(LocalVehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                hirepayment.BranchID = UserInfo.BranchID;
                LocalManifestModel.AddLocalVehicleHirePayment(loginID, hirepayment);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteLocalVehicleHire(int vehicleHireID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                LocalManifestModel.DeleteLocalVehicleHire(loginID, vehicleHireID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetLocalVehicleHirePayments()
        {
            try
            {
                int branchID = UserInfo.BranchID;
                return DataTableToJSON(LocalManifestModel.GetLocalVehicleHirePayments(branchID));
                //return  Json(ManifestModel.GetVehicleHirePayments(branchID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetLocalVehicleHirePaymentByID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetLocalVehicleHirePaymentByID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LOCAL VEHICLE MANIFEST
        public string GetConsignmentsForLocalManifest(int manifestId)
        {
            try
            {
                int branchID = UserInfo.BranchID;
                string bookingJSON = DataTableToJSON(LocalManifestModel.GetConsignmentsForLocalManifest(branchID, manifestId));
                //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLocalVehicleHireManifest(LocalVehicleHireManifest manifest)
        {
            try
            {
                int loginID = UserInfo.UserID;
                
                manifest.EstimatedDeliveryDate = System.DateTime.Now;//TEMPORARY ADDED
                LocalManifestModel.UpdateLocalVehicleHireManifest(loginID, manifest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddLocalVehicleHireManifest(LocalVehicleHireManifest manifest)
        {
            try
            {
                int loginID = UserInfo.UserID;
                int branchID = UserInfo.BranchID;
                
                manifest.EstimatedDeliveryDate = System.DateTime.Now;//TEMPORARY ADDED
                int manifestID = LocalManifestModel.AddLocalVehicleHireManifest(loginID, branchID, manifest);
                return manifestID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteLocalVehicleHireManifest(int manifestID)
        {
            try
            {
                int loginID = UserInfo.UserID;
                LocalManifestModel.DeleteLocalVehicleHireManifest(loginID, manifestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetLocalVehicleHireManifestByManifestID(int manifestID)
        {
            try
            {

                return Json(LocalManifestModel.GetLocalVehicleHireManifestByManifestID(manifestID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetLocalVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetLocalVehicleHireManifestByVehicleHireID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
        #region LOCAL MANIFEST CONSIGNMENTS
        public void AddLocalManifestConsignment(List<LocalManifestConsignmentMapping> manifestConsignments)
        {
            try
            {
                int loginID = UserInfo.UserID;

                LocalManifestModel.AddLocalManifestConsignment(loginID, manifestConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetLocalManifestConsignments(int manifestID)
        {
            try
            {

                var manifestConsignments = Json(LocalManifestModel.GetLocalManifestConsignments(manifestID), JsonRequestBehavior.AllowGet);
                return manifestConsignments;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UNLOAD MANIFEST CONSIGNMENTS
        public string GetManifestsForUnloadByToBranchID()
        {
            try
            {
                int branchID = UserInfo.BranchID;
                string bookingJSON = DataTableToJSON(ManifestModel.GetManifestsForUnloadByToBranchID(branchID));
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUnloadManifestConsignments(int manifestID)
        {
            try
            {
                return DataTableToJSON(ManifestModel.GetUnloadManifestConsignments(manifestID));
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManifestConsignmentStatus(List<ManifestConsignmentMapping> manifestConsignmentList)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ManifestModel.UpdateManifestConsignmentStatus(loginID, manifestConsignmentList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Delivery MANIFEST CONSIGNMENTS
        
        public string GetManifestConsignmentsForDelivery()
        {
            try
            {
                int branchID = UserInfo.BranchID;

                return DataTableToJSON(ManifestModel.GetManifestConsignmentsForDelivery(branchID));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManifestConsignmentDeliveryStatus(ManifestConsignmentMapping manifestConsignment)
        {
            try
            {
                int loginID = UserInfo.UserID;
                ManifestModel.UpdateManifestConsignmentDeliveryStatus(loginID, manifestConsignment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region Manifest Report
        public string GetManifestsForReport(int fromID ,int toID, string  createdDate)
        {
            try
            {
                System.Nullable<int> fromLoc = null;
                System.Nullable<int> toLoc = null;
                if (fromID > 0)
                    fromLoc = fromID;
                if (toID > 0)
                    toLoc = toID;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
                System.Nullable< DateTime> dtcreated= null;
                if (createdDate.Length > 0)
                { dtcreated = Convert.ToDateTime(createdDate,provider); }
                return DataTableToJSON(ManifestModel.GetManifestsForReport(fromLoc,toLoc, dtcreated));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}