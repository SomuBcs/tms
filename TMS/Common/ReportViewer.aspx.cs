﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TMS.Models;
namespace TMS.Common
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["manifestID"] != null)
                {
                    int  manifestID =Convert.ToInt32 (Request["manifestID"].ToString());
                    ViewState["manifestID"] = manifestID;
                    DataTable  dtManifest = ManifestModel.GetManifestReport(manifestID);
                    DataTable dtConsignments = ManifestModel.GetManifestConsignmentsForReports(manifestID);
                    rptvwrTMS.LocalReport.DataSources.Clear();
                    rptvwrTMS.LocalReport.DataSources.Add(new ReportDataSource("dsManifest", dtManifest));
                    rptvwrTMS.LocalReport.DataSources.Add(new ReportDataSource("dsConsignments", dtConsignments));

                    rptvwrTMS.LocalReport.ReportPath = Server.MapPath("~\\ReportRdls\\ManifestReport.rdlc");
                    rptvwrTMS.DocumentMapCollapsed = true;
                    this.rptvwrTMS.LocalReport.Refresh();
                }
            }
         }
    }
}