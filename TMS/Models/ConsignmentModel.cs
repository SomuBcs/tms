﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.EFModel;
using TMS.Helper;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace TMS.Models
{
    public static class ConsignmentModel
    {
        #region consignment booking
        internal static DataTable GetLocalVehiclesHired(int branchID)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehiclesHired";

                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsVehicles = new DataSet();
                da.Fill(dsVehicles, "table1");
                connection.Close();
                return dsVehicles.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        internal static DataTable GetConsignmentBookings(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookings";
                SqlParameter paramBrnch  = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;
                
                cmd.Parameters.Add(paramBrnch);
              
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static int  AddConsignmentBooking(int  loginID, ConsignmentBooking newBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBooking.CreatedBy = loginID;
                newBooking.Created = System.DateTime.Now;

                newBooking.ModifiedBy = loginID;
                newBooking.Modified = System.DateTime.Now;
                newBooking.ConsignmentNo = "";//AUTO GENERATED IN STORED PROC
                if (newBooking.ConsigneeID == null ) newBooking.ConsigneeID =0;
                if (newBooking.ConsignorID == null) newBooking.ConsignorID = 0;
                if (newBooking.ConsigneeAddress == null) newBooking.ConsigneeAddress = "";
                if (newBooking.ConsignorAddress == null) newBooking.ConsignorAddress = "";
               // if (newBooking.BillingPartyID == null) newBooking.BillingPartyID = 0;
                newBooking.ConsignmentDate = System.DateTime.Now;
                db.command.CommandText = "AddConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentNo", SqlDbType.Char).Value = newBooking.ConsignmentNo;
                db.command.Parameters.Add("@pConsigneeName", SqlDbType.VarChar).Value = newBooking.ConsigneeName;
                db.command.Parameters.Add("@pConsigneeAddress", SqlDbType.VarChar).Value = newBooking.ConsigneeAddress;
                db.command.Parameters.Add("@pConsigneeID", SqlDbType.Int).Value = newBooking.ConsigneeID;


                db.command.Parameters.Add("@pConsignorName", SqlDbType.VarChar).Value = newBooking.ConsignorName;
                db.command.Parameters.Add("@pConsignorAddress", SqlDbType.VarChar).Value = newBooking.ConsignorAddress;

                db.command.Parameters.Add("@pCompanyID", SqlDbType.Int ).Value = newBooking.CompanyID;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int ).Value = newBooking.BranchID;
                db.command.Parameters.Add("@pZoneID", SqlDbType.Int).Value = newBooking.ZoneID;


                db.command.Parameters.Add("@pConsignorID", SqlDbType.Int ).Value = newBooking.ConsignorID;

                db.command.Parameters.Add("@pConsignmentDate", SqlDbType.Date).Value = newBooking.ConsignmentDate;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int ).Value = newBooking.PaymentTypeID;

                db.command.Parameters.Add("@pConsignmentFromID", SqlDbType.Int).Value = newBooking.ConsignmentFromID;
                db.command.Parameters.Add("@pConsignmentToID", SqlDbType.Int).Value = newBooking.ConsignmentToID;
                db.command.Parameters.Add("@pDeliveryTypeID", SqlDbType.Int).Value = newBooking.DeliveryTypeID;
                db.command.Parameters.Add("@pBillingPartyID", SqlDbType.Int).Value = newBooking.BillingPartyID;
                db.command.Parameters.Add("@pTransportTypeID", SqlDbType.Int).Value = newBooking.TransportTypeID;
                db.command.Parameters.Add("@pServiceTaxId", SqlDbType.Int).Value = newBooking.ServiceTaxId;
                db.command.Parameters.Add("@pDoorCollectionID", SqlDbType.Int).Value = newBooking.DoorCollectionID;
                db.command.Parameters.Add("@pTransportModeID", SqlDbType.Int).Value = newBooking.TransportModeID;
                db.command.Parameters.Add("@pCargotTypeID", SqlDbType.Int).Value = newBooking.CargotTypeID;
                db.command.Parameters.Add("@pCreatedBy", SqlDbType.Int).Value = newBooking.CreatedBy;
                db.command.Parameters.Add("@pModifiedBy", SqlDbType.Int).Value = newBooking.ModifiedBy;
                db.command.Parameters.Add("@pVehicleID", SqlDbType.Int).Value = newBooking.VehicleID;
                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@pConsignmentID";
                paramConsID.Direction = ParameterDirection.Output;
                paramConsID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramConsID);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int consignmentID = Convert.ToInt32(paramConsID.Value);
                return consignmentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateConsignmentBooking(int loginID, ConsignmentBooking newBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBooking.CreatedBy = loginID;
                newBooking.Created = System.DateTime.Now;

                newBooking.ModifiedBy = loginID;
                newBooking.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsigneeName", SqlDbType.VarChar).Value = newBooking.ConsigneeName;
                db.command.Parameters.Add("@ConsigneeAddress", SqlDbType.VarChar).Value = newBooking.ConsigneeAddress;
                db.command.Parameters.Add("@ConsigneeID", SqlDbType.Int).Value = newBooking.ConsigneeID;

                db.command.Parameters.Add("@ConsignorName", SqlDbType.VarChar).Value = newBooking.ConsignorName;
                db.command.Parameters.Add("@ConsignorAddress", SqlDbType.VarChar).Value = newBooking.ConsignorAddress;
                db.command.Parameters.Add("@ConsignorID", SqlDbType.Int).Value = newBooking.ConsignorID;

               
                db.command.Parameters.Add("@ConsignmentDate", SqlDbType.Date).Value = newBooking.ConsignmentDate;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newBooking.PaymentTypeID;

                db.command.Parameters.Add("@ConsignmentFromID", SqlDbType.Int).Value = newBooking.ConsignmentFromID;
                db.command.Parameters.Add("@ConsignmentToID", SqlDbType.Int).Value = newBooking.ConsignmentToID;
                db.command.Parameters.Add("@DeliveryTypeID", SqlDbType.Int).Value = newBooking.DeliveryTypeID;
                db.command.Parameters.Add("@BillingPartyID", SqlDbType.Int).Value = newBooking.BillingPartyID;
                db.command.Parameters.Add("@TransportTypeID", SqlDbType.Int).Value = newBooking.TransportTypeID;
                db.command.Parameters.Add("@ServiceTaxId", SqlDbType.Int).Value = newBooking.ServiceTaxId;
                db.command.Parameters.Add("@DoorCollectionID", SqlDbType.Int).Value = newBooking.DoorCollectionID;
                db.command.Parameters.Add("@TransportModeID", SqlDbType.Int).Value = newBooking.TransportModeID;
                db.command.Parameters.Add("@CargotTypeID", SqlDbType.Int).Value = newBooking.CargotTypeID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newBooking.ModifiedBy;
                db.command.Parameters.Add("@pVehicleID", SqlDbType.Int).Value = newBooking.VehicleID;
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newBooking.ConsignmentID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteConsignmentBooking(int loginID, int consignmentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
               
                db.command.CommandText = "DeleteConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = consignmentID;
                db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<ConsignmentBooking> GetConsignmentBookingByID(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookingByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ConsignmentID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentBooking> list = new List<ConsignmentBooking>();
                return list = DatatableHelper.ToList<ConsignmentBooking>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion

        #region consignment Payment

        internal static double GetTotalBasicFreight(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetSumBasicFreight";
                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@ConsignmentID";
                paramConsID.Direction = ParameterDirection.Input;
                paramConsID.DbType = DbType.Int32;
                paramConsID.Value = consignmentID;

                cmd.Parameters.Add(paramConsID);

                connection.Open();
                var result = cmd.ExecuteScalar();

                double totalFreight = 0;
                if (result != System.DBNull.Value)
                    totalFreight = Convert.ToDouble(result);

                return totalFreight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static double GetInvoiceTotal(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetTotalInvoiceValue";
                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@ConsignmentID";
                paramConsID.Direction = ParameterDirection.Input;
                paramConsID.DbType = DbType.Int32;
                paramConsID.Value = consignmentID;

                cmd.Parameters.Add(paramConsID);

                connection.Open();
                var result = cmd.ExecuteScalar();

                double totalInvoice = 0;
                if (result != System.DBNull.Value)
                    totalInvoice = Convert.ToDouble(result);

                return totalInvoice;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static void AddConsignmentPayment(int loginID, ConsignmentBookingPayment paymentBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                paymentBooking.CreatedBy = loginID;
               
                paymentBooking.ModifiedBy = loginID;

                if (paymentBooking.RoadPermitAttached == null)
                    paymentBooking.RoadPermitAttached = false;
                if (paymentBooking.TotalAmount == null)
                    paymentBooking.TotalAmount = 0;
                if (paymentBooking.GrandTotal == null)
                    paymentBooking.GrandTotal = 0;

                db.command.CommandText = "AddConsignmentPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = paymentBooking.ConsignmentID;
                db.command.Parameters.Add("@GoodsDescription", SqlDbType.Int).Value = paymentBooking.GoodsDescription;
                db.command.Parameters.Add("@InsuranceFlag", SqlDbType.Bit).Value = paymentBooking.InsuranceFlag;
                db.command.Parameters.Add("@DocketCharges", SqlDbType.Float).Value = paymentBooking.DocketCharges;


                db.command.Parameters.Add("@FOV", SqlDbType.Float).Value = paymentBooking.FOV ;
                
                db.command.Parameters.Add("@DoorCollection", SqlDbType.Float).Value = paymentBooking.DoorCollection;
                db.command.Parameters.Add("@Octroicharges", SqlDbType.Float).Value = paymentBooking.Octroicharges;
                db.command.Parameters.Add("@DoorDelivery", SqlDbType.Float).Value = paymentBooking.DoorDelivery;


                db.command.Parameters.Add("@OctroiSurcharges", SqlDbType.Float).Value = paymentBooking.OctroiSurcharges;

                db.command.Parameters.Add("@LabourCharges", SqlDbType.Float).Value = paymentBooking.LabourCharges;
                db.command.Parameters.Add("@OtherCharges", SqlDbType.Float).Value = paymentBooking.OtherCharges;

                db.command.Parameters.Add("@TotalAmount", SqlDbType.Float).Value = paymentBooking.TotalAmount;
                //db.command.Parameters.Add("@TaxFlag", SqlDbType.Bit ).Value = paymentBooking.TaxFlag;
                db.command.Parameters.Add("@RoadPermitAttached", SqlDbType.Bit).Value = paymentBooking.RoadPermitAttached;
                db.command.Parameters.Add("@RoadPermitType", SqlDbType.VarChar).Value = paymentBooking.RoadPermitType;
                db.command.Parameters.Add("@Remark", SqlDbType.VarChar).Value = paymentBooking.Remark;
                db.command.Parameters.Add("@GrandTotal", SqlDbType.Float).Value = paymentBooking.GrandTotal;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = paymentBooking.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = paymentBooking.ModifiedBy;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymentBooking.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = paymentBooking.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = paymentBooking.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = paymentBooking.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = paymentBooking.PanNo;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }

        internal static void UpdateConsignmentPayment(int loginID, ConsignmentBookingPayment paymentBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {


                paymentBooking.ModifiedBy = loginID;


                db.command.CommandText = "UpdateConsignmentPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
               
                db.command.Parameters.Add("@GoodsDescription", SqlDbType.Int).Value = paymentBooking.GoodsDescription;
                db.command.Parameters.Add("@InsuranceFlag   ", SqlDbType.Bit).Value = paymentBooking.InsuranceFlag;
                db.command.Parameters.Add("@DocketCharges", SqlDbType.Float).Value = paymentBooking.DocketCharges;


                db.command.Parameters.Add("@FOV", SqlDbType.Float).Value = paymentBooking.FOV;

                db.command.Parameters.Add("@DoorCollection", SqlDbType.Float).Value = paymentBooking.DoorCollection;
                db.command.Parameters.Add("@Octroicharges", SqlDbType.Float).Value = paymentBooking.Octroicharges;
                db.command.Parameters.Add("@DoorDelivery", SqlDbType.Float).Value = paymentBooking.DoorDelivery;


                db.command.Parameters.Add("@OctroiSurcharges", SqlDbType.Float).Value = paymentBooking.OctroiSurcharges;

                db.command.Parameters.Add("@LabourCharges", SqlDbType.Float).Value = paymentBooking.LabourCharges;
                db.command.Parameters.Add("@OtherCharges", SqlDbType.Float).Value = paymentBooking.OtherCharges;

                db.command.Parameters.Add("@TotalAmount", SqlDbType.Float).Value = paymentBooking.TotalAmount;
               // db.command.Parameters.Add("@TaxFlag", SqlDbType.Bit).Value = paymentBooking.TaxFlag;
                
                db.command.Parameters.Add("@RoadPermitAttached", SqlDbType.Bit).Value = paymentBooking.RoadPermitAttached;
                db.command.Parameters.Add("@RoadPermitType", SqlDbType.VarChar).Value = paymentBooking.RoadPermitType;
                db.command.Parameters.Add("@Remark", SqlDbType.VarChar).Value = paymentBooking.Remark;
                db.command.Parameters.Add("@GrandTotal", SqlDbType.Float).Value = paymentBooking.GrandTotal;
                
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = paymentBooking.ModifiedBy;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymentBooking.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = paymentBooking.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = paymentBooking.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = paymentBooking.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = paymentBooking.PanNo;
                db.command.Parameters.Add("@BookingPaymentID", SqlDbType.Int).Value = paymentBooking.BookingPaymentID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteConsignmentBookingPayment(int loginID, int bookingPaymentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentBookingPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@bookingPaymentID", SqlDbType.Int).Value = bookingPaymentID;
                db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<ConsignmentBookingPayment> GetConsignmentBookingPaymentByID(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookingPaymentsByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ConsignmentID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentBookingPayment> list = new List<ConsignmentBookingPayment>();
                return list = DatatableHelper.ToList<ConsignmentBookingPayment>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion


        #region consignment Party Invoice
        internal static DataTable GetConsignmentPartyInvoices(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentPartyInvoices";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@consignmentID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = consignmentID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsPartyInvoice = new DataSet();
                da.Fill(dsPartyInvoice, "table1");
                connection.Close();
                return dsPartyInvoice.Tables[0];
           

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static void AddConsignmentPartyInvoice(int loginID, ConsignmentPartyInvoice partyInvoice)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                partyInvoice.CreatedBy = loginID;
                partyInvoice.Created = System.DateTime.Now;

                partyInvoice.ModifiedBy = loginID;
                partyInvoice.Modified = System.DateTime.Now;

              ;
                db.command.CommandText = "AddConsignmentPartyInvoice";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int ).Value = partyInvoice.ConsignmentID;
                db.command.Parameters.Add("@InvoiceNo", SqlDbType.VarChar ).Value = partyInvoice.InvoiceNo;
                db.command.Parameters.Add("@InvoiceDate", SqlDbType.Date).Value = partyInvoice.InvoiceDate;
                db.command.Parameters.Add("@InvoiceQuantity", SqlDbType.Int ).Value = partyInvoice.InvoiceQuantity;


                db.command.Parameters.Add("@InvoiceValue", SqlDbType.Float).Value = partyInvoice.InvoiceValue;
                db.command.Parameters.Add("@PartNo", SqlDbType.VarChar).Value = partyInvoice.PartNo;

                db.command.Parameters.Add("@ASNNo", SqlDbType.VarChar ).Value = partyInvoice.ASNNo;
                db.command.Parameters.Add("@Article", SqlDbType.Int).Value = partyInvoice.Article;
                db.command.Parameters.Add("@PackagingID", SqlDbType.Int).Value = partyInvoice.PackagingID;


                db.command.Parameters.Add("@ActualWeight", SqlDbType.Float ).Value = partyInvoice.ActualWeight;

                db.command.Parameters.Add("@ChargeWeight", SqlDbType.Float).Value = partyInvoice.ChargeWeight;
                db.command.Parameters.Add("@RatePerKG", SqlDbType.Float).Value = partyInvoice.RatePerKG;

                db.command.Parameters.Add("@BasicFreight", SqlDbType.Float).Value = partyInvoice.BasicFreight;
                db.command.Parameters.Add("@ChargeType", SqlDbType.Int).Value = partyInvoice.ChargeType;

                db.command.Parameters.Add("@LxWxH", SqlDbType.VarChar ).Value = partyInvoice.LxWxH;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = partyInvoice.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = partyInvoice.ModifiedBy;
                db.command.Parameters.Add("@PONo", SqlDbType.VarChar).Value = partyInvoice.PONo;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
 
        internal static void DeleteConsignmentPartyInvoice(int loginID, int partyInvoiceID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentPartyInvoice";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PartyInvoiceID", SqlDbType.Int).Value = partyInvoiceID;
                

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        #endregion


        #region MASTER DATA RETREIVAL
        internal static List<VehicleMaster> GetVehicleByID(int vehicleID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleByID";
                SqlParameter paramVehID = new SqlParameter();
                paramVehID.ParameterName = "@VehicleID";
                paramVehID.Direction = ParameterDirection.Input;
                paramVehID.DbType = DbType.Int32;
                paramVehID.Value = vehicleID;

                cmd.Parameters.Add(paramVehID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsVehicle= new DataSet();
                da.Fill(dsVehicle, "table1");
                connection.Close();

                List<VehicleMaster> list = new List<VehicleMaster>();
                return list = DatatableHelper.ToList<VehicleMaster>(dsVehicle.Tables[0]);
           
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static object GetBranches()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<BranchMaster> list = new List<BranchMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetBranches");
                return list = DatatableHelper.ToList<BranchMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetCargoTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<CargoTypeMaster> list = new List<CargoTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetCargoTypes");
                return list = Helper.DatatableHelper.ToList<CargoTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetChargeTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ChargeTypeMaster> list = new List<ChargeTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetChargeTypes");
                return list = DatatableHelper.ToList<ChargeTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetCompanies()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<CompanyMaster> list = new List<CompanyMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetCompanies");
                return list = DatatableHelper.ToList<CompanyMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static String GetNextConsignmentNo(int branchID)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetNextConsignmentNo";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                

                cmd.Parameters.Add(paramBrnch);
                connection.Open();
                object retVal = cmd.ExecuteScalar();
                
                String  consignNo = cmd.ExecuteScalar().ToString();
                connection.Close();
                return consignNo;

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static String GetNextRunningNo(int branchID,string TransactionType)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetNextRunningNo";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramTrans = new SqlParameter();
                paramTrans.ParameterName = "@TransactionType";
                paramTrans.Direction = ParameterDirection.Input;
                paramTrans.DbType = DbType.String;
                paramTrans.Value = TransactionType;



                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramTrans);
                connection.Open();
                object retVal = cmd.ExecuteScalar();

                String consignNo = cmd.ExecuteScalar().ToString();
                connection.Close();
                return consignNo;

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetVehicles(bool isVehicleHired )
        {
            try
            {
                if (isVehicleHired == null)
                    isVehicleHired = false;

                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicles";
                SqlParameter paramVehHire = new SqlParameter();
                paramVehHire.ParameterName = "@Ishired";
                paramVehHire.Direction = ParameterDirection.Input;
                paramVehHire.DbType = DbType.Boolean;
                paramVehHire.Value = isVehicleHired;

                cmd.Parameters.Add(paramVehHire);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<VehicleMaster> list = new List<VehicleMaster>();
                return list = DatatableHelper.ToList<VehicleMaster>(ds.Tables[0]);

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        } 
        internal static object GetDeliveryTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DeliveryTypeMaster> list = new List<DeliveryTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDeliveryTypes");
                return list = DatatableHelper.ToList<DeliveryTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetDoorCollections()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DoorCollectionMaster> list = new List<DoorCollectionMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDoorCollections");
                return list =DatatableHelper.ToList<DoorCollectionMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetPackagings()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PackagingMaster> list = new List<PackagingMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPackagings");
                return list =DatatableHelper.ToList<PackagingMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static object GetPaymentTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PaymentTypeMaster> list = new List<PaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPaymentTypes");
                return list = DatatableHelper.ToList<PaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetTaxes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TaxMaster> list = new List<TaxMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTaxes");
                return list = DatatableHelper.ToList<TaxMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetGoods()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<GoodsMaster> list = new List<GoodsMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetGoods");
                return list = DatatableHelper.ToList<GoodsMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetServiceTaxpaidBy()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ServiceTaxPaidByMaster> list = new List<ServiceTaxPaidByMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetServiceTaxPaidBy");
                return list = DatatableHelper.ToList<ServiceTaxPaidByMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetTransportModes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TransportmodeMaster> list = new List<TransportmodeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTransportModes");
                return list = DatatableHelper.ToList<TransportmodeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static object GetTransportTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TransportTypeMaster> list = new List<TransportTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTransportTypes");
                return list =  DatatableHelper.ToList<TransportTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorMaster> list = new List<VendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVendors");
                return list =  DatatableHelper.ToList<VendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetZones()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ZoneMaster> list = new List<ZoneMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetZones");
                return list =  DatatableHelper.ToList<ZoneMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        #endregion
    }
}