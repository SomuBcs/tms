﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.Helper;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TMS.Models
{
    public static class ManifestModel
    {
        #region VEHICLE MASTER
        internal static void AddVehicle(int loginID, VehicleMaster newVehicle)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicle.CreatedBy = loginID;
                newVehicle.Created = System.DateTime.Now;

                newVehicle.ModifiedBy = loginID;
                newVehicle.Modified = System.DateTime.Now;
                // if (newVehicle.VehicleNo == null) newVehicle.VehicleNo = "";
                db.command.CommandText = "AddVehicle";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleName", SqlDbType.VarChar).Value = newVehicle.VehicleName;
                db.command.Parameters.Add("@VehicleNo", SqlDbType.VarChar).Value = newVehicle.VehicleNo;
                db.command.Parameters.Add("@VehicleType", SqlDbType.VarChar).Value = newVehicle.VehicleType;

                db.command.Parameters.Add("@RegistrationDate", SqlDbType.Date).Value = newVehicle.RegistrationDate;
                db.command.Parameters.Add("@FitnessExpDate ", SqlDbType.Date).Value = newVehicle.FitnessExpDate;


                db.command.Parameters.Add("@InsuranceExpDate", SqlDbType.Date).Value = newVehicle.InsuranceExpDate;
                db.command.Parameters.Add("@PUCExpDate", SqlDbType.Date).Value = newVehicle.PUCExpDate;
                db.command.Parameters.Add("@EngineNo", SqlDbType.VarChar).Value = newVehicle.EngineNo;

                db.command.Parameters.Add("@ChasisNo", SqlDbType.VarChar).Value = newVehicle.ChasisNo;

                db.command.Parameters.Add("@OwnerType", SqlDbType.VarChar).Value = newVehicle.OwnerType;
                db.command.Parameters.Add("@BrokerName", SqlDbType.VarChar).Value = newVehicle.BrokerName;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicle.VendorName;
                db.command.Parameters.Add("@loadingCapacity", SqlDbType.VarChar).Value = newVehicle.loadingCapacity;
                db.command.Parameters.Add("@MaxloadingCapacity", SqlDbType.VarChar).Value = newVehicle.MaxloadingCapacity;
                db.command.Parameters.Add("@isHiredVehicle", SqlDbType.Bit).Value = newVehicle.isHiredVehicle;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicle.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicle.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Vehicle Hire Payment
        internal static DataTable GetVehicleHirePayments(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHirePayment";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
             //   List<VehicleHirePayment> list = new List<VehicleHirePayment>();
             // return list =  DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static void  AddVehicleHirePayment(int loginID, VehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;
                newVehicleHire.HireDate = System.DateTime.Now;
                // if (newVehicleHire.VehicleNo == null) newVehicleHire.VehicleNo = "";
                db.command.CommandText = "AddVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = newVehicleHire.BranchID;
                db.command.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = newVehicleHire.HireDate;
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;


                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;


                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = newVehicleHire.Remarks;
                
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicleHire.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
               

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                Utility.LogError(ex, "AddVehicleHirePayment", loginID);
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVehicleHirePayment(int loginID, VehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;

                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;

                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newVehicleHire.VehicleHireID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteVehicleHire(int loginID, int vehicleHireID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = vehicleHireID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<VehicleHirePayment> GetVehicleHirePaymentByID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHirePaymentByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VehicleHireID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = vehicleHireID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleHirePayment> list = new List<VehicleHirePayment>();
                return list =  DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion

        #region Vehicle Hire Manifest
        internal static DataTable GetConsignmentsForManifest(int branchID,int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForManifest";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// M
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        internal static List<ManifestConsignmentMapping> GetManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<ManifestConsignmentMapping> list = new List<ManifestConsignmentMapping>();
                return list =  DatatableHelper.ToList<ManifestConsignmentMapping>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static List<VehicleHireManifest> GetVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHireManifestByVehicleHireID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@VehicleHireID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = vehicleHireID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<VehicleHireManifest> list = new List<VehicleHireManifest>();
                return list =  DatatableHelper.ToList<VehicleHireManifest>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static int  AddVehicleHireManifest(int loginID, int branchID, VehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newManifest.CreatedBy = loginID;
                newManifest.Created = System.DateTime.Now;

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "AddVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newManifest.VehicleHireID;
                
                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;

                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime ).Value = newManifest.EstimatedDeliveryDate;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;


                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc ;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID";
                paramMFID.Direction = ParameterDirection.Output;
                paramMFID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramMFID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int manifestID= Convert.ToInt32(paramMFID.Value);
                return manifestID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVehicleHireManifest(int loginID, VehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
               
                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;

                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = newManifest.EstimatedDeliveryDate;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;

                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifest.ManifestID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
    
        
  
        internal static void DeleteVehicleHireManifest(int loginID, int manifestID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<VehicleHireManifest> GetVehicleHireManifestByManifestID(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHireManifestByManifestID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ManifestID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = manifestID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleHireManifest> list = new List<VehicleHireManifest>();
                return list =  DatatableHelper.ToList<VehicleHireManifest>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion
        
        #region MASTERS
        internal static object GetVehicleHirePaymentTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VehicleHirePaymentTypeMaster> list = new List<VehicleHirePaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVehicleHirePaymentTypes");
                return list =  DatatableHelper.ToList<VehicleHirePaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetTruckBookingTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TruckBookingTypeMaster> list = new List<TruckBookingTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTruckBookingTypes");
                return list =  DatatableHelper.ToList<TruckBookingTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetDeliveryStatusMaster()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DeliveryStatusMaster> list = new List<DeliveryStatusMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDeliveryStatusMaster");
                return list = DatatableHelper.ToList<DeliveryStatusMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetVendorTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorTypeMaster> list = new List<VendorTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVendorTypes");
                return list =  DatatableHelper.ToList<VendorTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        #endregion
        
        #region manifest consignmets
        internal static void AddManifestConsignment(int loginID, List<ManifestConsignmentMapping> ManifestConsignList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction()) {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteManifestConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = ManifestConsignList[0].ManifestID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (ManifestConsignmentMapping newManifestConsign in ManifestConsignList)
                    {

                        newManifestConsign.CreatedBy = loginID;
                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddManifestConsignments";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifestConsign.ManifestID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newManifestConsign.ConsignmentID;
                        addCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifestConsign.CreatedBy;
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
                }


        }



        #endregion

        #region Manifest Delivery
        internal static DataTable GetManifestConsignmentsForDelivery(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignmentsForDelivery";
               

                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID ";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

               
                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateManifestConsignmentDeliveryStatus(int loginID, ManifestConsignmentMapping manifestConsignment)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            

                try
                {
                        SqlCommand updateCmd = new SqlCommand();
                        updateCmd.CommandText = "UpdateManifestConsignmentDeliveryStatus";
                        updateCmd.CommandType = CommandType.StoredProcedure;

                        updateCmd.Parameters.Add("@ManifestConsignmentID", SqlDbType.Int).Value = manifestConsignment.ManifestConsignmentID;
                        updateCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestConsignment.ManifestID;
                        updateCmd.Parameters.Add("@DeliveryStatus", SqlDbType.Int).Value = manifestConsignment.DeliveryStatus;
                        updateCmd.Parameters.Add("@DeliveryDate", SqlDbType.VarChar).Value = manifestConsignment.DeliveryDate;
                        updateCmd.Parameters.Add("@DeliveredArticles", SqlDbType.Int ).Value = manifestConsignment.DeliveredArticles;
                        updateCmd.Parameters.Add("@DeliveryRemarks", SqlDbType.VarChar).Value = manifestConsignment.DeliveryRemarks;
                        updateCmd.Connection = connection;
                        connection.Open();
                        updateCmd.ExecuteNonQuery();


                    
                    connection.Close();

                }///end try
                catch (Exception ex)
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }

            
        }
        #endregion

        #region Manifest Unload
        internal static DataTable GetManifestsForUnloadByToBranchID(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsForUnloadByToBranchID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@To_LocationID ";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static object GetConsignmentReceivedStatus()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ConsignmentReceivedStatu> list = new List<ConsignmentReceivedStatu>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetConsignmentReceivedStatus");
                return list = DatatableHelper.ToList<ConsignmentReceivedStatu>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static DataTable GetUnloadManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetUnloadManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static void UpdateManifestConsignmentStatus(int loginID, List<ManifestConsignmentMapping> manifestConsignmentList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {
                    int manifestID = manifestConsignmentList[0].ManifestID;
                    SqlCommand mfCmd = new SqlCommand();
                    mfCmd.CommandText = "UnloadVehicleHireManifest";
                    mfCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();

                    mfCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                    mfCmd.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                    mfCmd.Connection = connection;
                    mfCmd.Transaction = trans;
                    mfCmd.ExecuteNonQuery();

                    foreach (ManifestConsignmentMapping consignmentMapping in manifestConsignmentList)
                    {
                        SqlCommand updateCmd = new SqlCommand();
                        updateCmd.CommandText = "UpdateManifestConsignmentStatus";
                        updateCmd.CommandType = CommandType.StoredProcedure;

                        updateCmd.Parameters.Add("@ManifestConsignmentID", SqlDbType.Int).Value = consignmentMapping.ManifestConsignmentID;
                        updateCmd.Parameters.Add("@ReceivedArticles", SqlDbType.Int).Value = consignmentMapping.ReceivedArticles;
                        updateCmd.Parameters.Add("@ReceivedStatus", SqlDbType.VarChar).Value = consignmentMapping.ReceivedStatus;
                        updateCmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = consignmentMapping.Remarks;
                        updateCmd.Connection = connection;
                        updateCmd.Transaction = trans;
                        updateCmd.ExecuteNonQuery();


                    }
                    trans.Commit();
                    connection.Close();

                }///end try
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }

            }
        }
        #endregion
        #region Manifest Report
        internal static DataTable GetManifestsForReport(System.Nullable<int> frombranchId, System.Nullable<int> tobranchID,System.Nullable< DateTime> createdDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsForReport";
                SqlParameter paramFromBrch = new SqlParameter();
                paramFromBrch.ParameterName = "@From_LocationID ";
                paramFromBrch.Direction = ParameterDirection.Input;
                paramFromBrch.DbType = DbType.Int32;
                paramFromBrch.Value = frombranchId;

                SqlParameter paramToBrnch = new SqlParameter();
                paramToBrnch.ParameterName = "@To_LocationID";
                paramToBrnch.Direction = ParameterDirection.Input;
                paramToBrnch.DbType = DbType.Int32;
                paramToBrnch.Value = tobranchID;

                SqlParameter paramCreatedDt = new SqlParameter();
                paramCreatedDt.ParameterName = "@Created";
                paramCreatedDt.Direction = ParameterDirection.Input;
                paramCreatedDt.DbType = DbType.DateTime;
                paramCreatedDt.Value = createdDate ;

                cmd.Parameters.Add(paramFromBrch);
                cmd.Parameters.Add(paramToBrnch);
                cmd.Parameters.Add(paramCreatedDt);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataTable   GetManifestReport(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsReport";
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID ";
                paramMFID.Direction = ParameterDirection.Input;
                paramMFID.DbType = DbType.Int32;
                paramMFID.Value = manifestID;
                

                cmd.Parameters.Add(paramMFID);
                
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static DataTable  GetManifestConsignmentsForReports(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignmentsForReports";
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID ";
                paramMFID.Direction = ParameterDirection.Input;
                paramMFID.DbType = DbType.Int32;
                paramMFID.Value = manifestID;


                cmd.Parameters.Add(paramMFID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}