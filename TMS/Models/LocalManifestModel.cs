﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.Helper;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TMS.Models
{
    public class LocalManifestModel
    {


        #region Local Vehicle Hire Payment
        internal static DataTable GetLocalVehicleHirePayments(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHirePayment";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
                //   List<VehicleHirePayment> list = new List<VehicleHirePayment>();
                // return list =  DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static void AddLocalVehicleHirePayment(int loginID, LocalVehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;
                newVehicleHire.HireDate = System.DateTime.Now;
                db.command.CommandText = "AddLocalVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = newVehicleHire.BranchID;
                db.command.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = newVehicleHire.HireDate;
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;

                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;

                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = newVehicleHire.Remarks;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicleHire.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteLocalVehicleHire(int loginID, int vehicleHireID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteLocalVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = vehicleHireID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateLocalVehicleHirePayment(int loginID, LocalVehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateLocalVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;
                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;


                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newVehicleHire.LocalVehicleHireID; 
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static List<LocalVehicleHirePayment> GetLocalVehicleHirePaymentByID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHirePaymentByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VehicleHireID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = vehicleHireID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<LocalVehicleHirePayment> list = new List<LocalVehicleHirePayment>();
                return list = DatatableHelper.ToList<LocalVehicleHirePayment>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion

        #region Vehicle Hire Manifest
        internal static DataTable GetConsignmentsForLocalManifest(int branchID, int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForLocalManifest";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// M
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        internal static List<LocalManifestConsignmentMapping> GetLocalManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<LocalManifestConsignmentMapping> list = new List<LocalManifestConsignmentMapping>();
                return list =  DatatableHelper.ToList<LocalManifestConsignmentMapping>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static List<LocalVehicleHireManifest> GetLocalVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHireManifestByVehicleHireID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@VehicleHireID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = vehicleHireID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<LocalVehicleHireManifest> list = new List<LocalVehicleHireManifest>();
                return list =  DatatableHelper.ToList<LocalVehicleHireManifest>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static int AddLocalVehicleHireManifest(int loginID, int branchID, LocalVehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newManifest.CreatedBy = loginID;
                newManifest.Created = System.DateTime.Now;

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "AddLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newManifest.VehicleHireID;

                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;

                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = newManifest.EstimatedDeliveryDate;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;


                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID";
                paramMFID.Direction = ParameterDirection.Output;
                paramMFID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramMFID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int manifestID = Convert.ToInt32(paramMFID.Value);
                return manifestID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateLocalVehicleHireManifest(int loginID, LocalVehicleHireManifest  newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestNo", SqlDbType.DateTime).Value = newManifest.ManifestNo;


                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;

                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = newManifest.EstimatedDeliveryDate;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;

                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteLocalVehicleHireManifest(int loginID, int manifestID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<LocalVehicleHireManifest> GetLocalVehicleHireManifestByManifestID(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHireManifestByManifestID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ManifestID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = manifestID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<LocalVehicleHireManifest> list = new List<LocalVehicleHireManifest>();
                return list =  DatatableHelper.ToList<LocalVehicleHireManifest>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion
 
        #region manifest consignmets
        internal static void AddLocalManifestConsignment(int loginID, List<LocalManifestConsignmentMapping> ManifestConsignList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteLocalManifestConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = ManifestConsignList[0].ManifestID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (LocalManifestConsignmentMapping newManifestConsign in ManifestConsignList)
                    {

                        newManifestConsign.CreatedBy = loginID;
                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddLocalManifestConsignments";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifestConsign.ManifestID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newManifestConsign.ConsignmentID;
                        addCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifestConsign.CreatedBy;
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
            }


        }



        #endregion
    }
}