﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.BusinessEntities
{
    public static class UserInfo
    {
        public static int UserID { get; set; }
        public static string UserName { get; set; }
        public static string Address { get; set; }
        public static string Pincode { get; set; }
        public static string city { get; set; }
        public static string state { get; set; }
        public static string Nationalty { get; set; }
        public static string EmailID { get; set; }
        public static string Password { get; set; }
        public static string ContactNo { get; set; }
        public static int ZoneID { get; set; }
        public static int BranchID { get; set; }
        public static int RoleID { get; set; }
        public static int ReportingManagerID { get; set; }
        public static int DepartmentID { get; set; }
        public static bool HO { get; set; }
        public static bool Status { get; set; }
    }
}