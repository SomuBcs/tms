﻿App.controller('Consignment_ngController', ["$scope", "$http", "$filter","filterFilter", "Notification", function ($scope, $http, $filter,filterFilter, Notification) {
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.BranchId = '';
    $scope.consignmentBooking = {
        ConsignmentID: '',
        ConsignmentNo: '',
        ConsigneeName: '',
        ConsigneeAddress : '',
        CompanyID: '',
        BranchID: '',
        ZoneID: '',
        ConsigneeID: '',
        ConsignorName: '',
        ConsignorAddress: '',
        ConsignorID: '',
        ConsignmentDate: $scope.currentDate,
        PaymentTypeID: '',
        ConsignmentFromID: '',
        ConsignmentToID: '',
        DeliveryTypeID: '',
        BillingPartyID: '',
        TransportTypeID: '',
        ServiceTaxId: '',
        DoorCollectionID: '',
        TransportModeID: '',
        CargotTypeID: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        Status: '',
        VehicleID: ''
    };
    
    $scope.FormatDateOld = function (dateValue) {
        if (dateValue != null) {
            var dt = new Date(parseInt(dateValue.substr(6, 13)));
            return (((dt.getDate() < 10) ? "0" + dt.getDate() : dt.getDate())) + "/" + (((dt.getMonth() + 1) < 10) ? "0" + (dt.getMonth() + 1) : (dt.getMonth() + 1)) + "/" + dt.getFullYear();

        }
    };
    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }
    /*
    $('#txtConsignmentDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate:'-1d',
        // endDate: '0d',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtConsignmentDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    */


    $scope.activeConsignmentBooking= [];
    $scope.activeConsignmentBookingCollection = [];
    $scope.Branches = [];
    $scope.Companies= [];
    $scope.Zones = [];
    $scope.DeliveryTypes= [];
    $scope.PaymentTypes = [];
    $scope.CargoTypes = [];
    $scope.DoorCollections = [];
    $scope.Packagings = [];
    $scope.Transportmodes = [];
    $scope.TransportTypes = [];
    $scope.Vendors = [];
    $scope.ServiceTaxPaidBy = [];

    $scope.GetBranches = function () {

        //$scope.activetrainingOpportunities = '';
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             brnches.push(response.data[i]);
         }
         $scope.Branches =  brnches;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    
    $scope.GetCargoTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var crgoTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetCargoTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             crgoTypes.push(response.data[i]);
         }
         $scope.CargoTypes = crgoTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.GetCompanies = function () {

        //$scope.activetrainingOpportunities = '';
        var cmpanies = [];
        $http({
            method: 'GET',
            url: '/Operations/GetCompanies',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             Companies.push(response.data[i]);
         }
         $scope.CargotTypes = cmpanies;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetDeliveryTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var delvTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetDeliveryTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             delvTypes.push(response.data[i]);
         }
         $scope.DeliveryTypes = delvTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetDoorCollections = function () {

        //$scope.activetrainingOpportunities = '';
        var dcColl = [];
        $http({
            method: 'GET',
            url: '/Operations/GetDoorCollections',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             dcColl.push(response.data[i]);
         }
         $scope.DoorCollections = dcColl;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetPackagings = function () {

        //$scope.activetrainingOpportunities = '';
        var pkgs = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPackagings',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             pkgs.push(response.data[i]);
         }
         $scope.Packagings = pkgs;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetPaymentTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             pmtTypes.push(response.data[i]);
         }
         $scope.PaymentTypes = pmtTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetServiceTaxpaidBy = function () {

        //$scope.activetrainingOpportunities = '';
        var taxes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetServiceTaxpaidBy',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             taxes.push(response.data[i]);
         }
         $scope.ServiceTaxPaidBy = taxes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetTransportModes = function () {

        var transmode = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTransportModes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             transmode.push(response.data[i]);
         }
         $scope.Transportmodes = transmode;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetTransportTypes = function () {

        var trnptType = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTransportTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             trnptType.push(response.data[i]);
         }
         $scope.TransportTypes = trnptType;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.consignorVendors = [];
    $scope.BillingVendors = [];
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             vendor.push(response.data[i]);
         }
         $scope.Vendors = vendor;
         $scope.consignorVendors = vendor;
         $scope.BillingVendors = vendor;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.Vehicles = [];
    $scope.GetVehicles = function () {

        //$scope.activetrainingOpportunities = '';
        var vehList = [];
        $http({
            method: 'GET',
            url: '/Operations/GetLocalVehiclesHired',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             vehList.push(response.data[i]);
         }
         $scope.Vehicles = vehList;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.GetZones = function () {

        var zone = [];
        $http({
            method: 'GET',
            url: '/Operations/GetZones',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             zone.push(response.data[i]);
         }
         $scope.Zones = zone;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.GetNextConsignmentNo = function () {

        var branchID = $('#hdnBranchID').attr("value");;

        $http({
            method: 'GET',
            params: { 'branchID': branchID },
            url: '/Operations/GetNextConsignmentNo',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         $scope.consignmentBooking.ConsignmentNo = response.data;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.loadConsignmentBookings= function () {

        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
          //  params: { 'branchID': branchID, },
            url: '/Operations/GetConsignmentBookings',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             bookings.push(response.data[i]);
         }
         $scope.activeConsignmentBooking = bookings;
         $scope.activeConsignmentBookingCollection = bookings;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.isDoorCollection = false;
    $scope.ShowVehicles = function()
    {
        var doorCollectionID = $scope.consignmentBooking.DoorCollectionID;
        var objDoorColl = filterFilter($scope.DoorCollections, { DoorCollectionID: doorCollectionID });
        if (objDoorColl != null)
        {
            if (objDoorColl[0].DoorCollection == 'Yes')
            {
                $scope.isDoorCollection = true;
            }
            else
            {$scope.isDoorCollection = false;}
        }
    }
    $scope.isBillingParty= false;
    $scope.ShowBillingParty = function () {
        var ptID = $scope.consignmentBooking.PaymentTypeID;
        var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
        if (objPaymentType != null) {
            if (objPaymentType[0].PaymentType == 'Billing') {
                $scope.isBillingParty = true;
            }
            else { $scope.isBillingParty = false; }
        }
    }

    $scope.GetConsignorAddress = function () {
        try{
            var cnName = $scope.consignmentBooking.ConsignorID ;
            var vendorCon = filterFilter($scope.consignorVendors, { VendorID: cnName });
            if (vendorCon.length > 0) {
                $scope.consignmentBooking.ConsignorAddress = vendorCon[0].Address;
                $scope.consignmentBooking.ConsignorName = vendorCon[0].VendorName;
            }
        }
        catch(ex)
        {
            throw ex;
        }
    }

    $scope.GetConsigneeAddress = function () {
        try {
            var cnName = $scope.consignmentBooking.ConsigneeID ;
            var vendorConsinee = filterFilter($scope.Vendors, { VendorID: cnName });
            if (vendorConsinee.length > 0) {
                $scope.consignmentBooking.ConsigneeAddress = vendorConsinee[0].Address;
                $scope.consignmentBooking.ConsigneeName = vendorConsinee[0].VendorName;
            }
        }
        catch (ex) {
            throw ex;
        }
    }
    $scope.clearBookings = function () {
        $scope.consignmentBooking = {
            ConsignmentID: '',
            ConsignmentNo: '',
            ConsigneeName: '',
            ConsigneeAddress: '',
            CompanyID: '',
            BranchID: '',
            ZoneID: '',
            ConsigneeID: '',
            ConsignorName: '',
            ConsignorAddress: '',
            ConsignorID: '',
            ConsignmentDate: $scope.currentDate,
            PaymentTypeID: '',
            ConsignmentFromID: '',
            ConsignmentToID: '',
            DeliveryTypeID: '',
            BillingPartyID: '',
            TransportTypeID: '',
            ServiceTaxId: '',
            DoorCollectionID: '',
            TransportModeID: '',
            CargotTypeID: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            Status: '',
            VehicleID: ''
        };

    }

    $scope.GetConsignmentBookingByID = function (consignmentID) {

        var booking = [];
        $http({
            method: 'GET',
            params: { 'consignmentID': consignmentID, },
            url: '/Operations/GetConsignmentBookingByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             booking.push(response.data[i]);
         }
         
             $scope.consignmentBooking.ConsignmentID = booking[0].ConsignmentID;
             $scope.consignmentBooking.ConsignmentNo = booking[0].ConsignmentNo;
             $scope.consignmentBooking.ConsigneeName = booking[0].ConsigneeName;
             $scope.consignmentBooking.ConsigneeAddress = booking[0].ConsigneeAddress;
             $scope.consignmentBooking.CompanyID = booking[0].CompanyID;
             $scope.consignmentBooking.BranchID = booking[0].BranchID;
             $scope.consignmentBooking.ZoneID  = booking[0].ZoneID;
             $scope.consignmentBooking.ConsigneeID = booking[0].ConsigneeID;
             $scope.consignmentBooking.ConsignorName = booking[0].ConsignorName;
             $scope.consignmentBooking.ConsignorAddress = booking[0].ConsignorAddress;
             $scope.consignmentBooking.ConsignorID = booking[0].ConsignorID;
             $scope.consignmentBooking.ConsignmentDate =$filter('date')( $scope.FormatDate(booking[0].ConsignmentDate),'dd/MM/yyyy'); 
             $scope.consignmentBooking.PaymentTypeID = booking[0].PaymentTypeID;
             $scope.consignmentBooking.ConsignmentFromID = booking[0].ConsignmentFromID;
             $scope.consignmentBooking.ConsignmentToID = booking[0].ConsignmentToID;
             $scope.consignmentBooking.DeliveryTypeID = booking[0].DeliveryTypeID;
             $scope.consignmentBooking.BillingPartyID = booking[0].BillingPartyID;
             $scope.consignmentBooking.TransportTypeID = booking[0].TransportTypeID;
             $scope.consignmentBooking.ServiceTaxId = booking[0].ServiceTaxId;
             $scope.consignmentBooking.DoorCollectionID = booking[0].DoorCollectionID;
             $scope.consignmentBooking.TransportModeID = booking[0].TransportModeID;
             $scope.consignmentBooking.CargotTypeID = booking[0].CargotTypeID;
             if (booking[0].VehicleID != null )
                $scope.consignmentBooking.VehicleID = booking[0].VehicleID;

             $scope.ShowBillingParty()
             $scope.ShowVehicles()
             $scope.GetConsigneeAddress()
             $scope.GetConsignorAddress()
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.AddBookings = function () {
        $scope.clearBookings();
        $scope.GetBranches();
        $scope.GetCargoTypes();
        $scope.GetCompanies();
        $scope.GetDeliveryTypes();
        $scope.GetDoorCollections();
        $scope.GetPackagings();
        $scope.GetPaymentTypes();
        $scope.GetServiceTaxpaidBy();
        $scope.GetTransportModes();
        $scope.GetTransportTypes();
        $scope.GetVendors();
        $scope.GetVehicles();
     //   $scope.GetNextConsignmentNo();
        $('#ConsignmentModal').modal('show');
    }


    $scope.EditBookings = function (consignmentID) {
        $scope.clearBookings();
        $scope.GetBranches();
        $scope.GetCargoTypes();
        $scope.GetCompanies();
        $scope.GetDeliveryTypes();
        $scope.GetDoorCollections();
        $scope.GetPackagings();
        $scope.GetPaymentTypes();
        $scope.GetServiceTaxpaidBy();
        $scope.GetTransportModes();
        $scope.GetTransportTypes();
        $scope.GetVendors();
        $scope.GetVehicles();
        $scope.GetConsignmentBookingByID(consignmentID)
        // $scope.GetNextConsignmentNo();

        $('#ConsignmentModal').modal('show');
    }

    
    $scope.DeleteBookings = function (consignmentID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteConsignmentBooking',
                params: { 'consignID': consignmentID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
              Notification.success({ message: "Consignment Booking deleted successfully.", delay: 5000 });
              $scope.loadConsignmentBookings();
          }, function errorCallback(response) {
              var e = response.message;
          });
        }
    }
    $scope.SaveBookings = function () {

        if ($scope.frmBookings.$valid) {
            if ($scope.consignmentBooking.ConsignmentID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddConsignmentBooking',
                    data: JSON.stringify($scope.consignmentBooking),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                 
                 Notification.success({ message: "Consignment Booking saved successfully.", delay: 5000 });
                 //$scope.clearBookings();
                 var consignmentID = response.data;

                 $scope.consignmentBooking.ConsignmentID = consignmentID;
                 $scope.showPartyInvoiceModal();

                 
             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateConsignmentBooking',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.consignmentBooking),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
               .then(function successCallback(response) {
                   console.log(response);
                   $scope.clearBookings();
                   $('#ConsignmentModal').modal('hide');

                   Notification.success({ message: "Consignment Booking saved successfully.", delay: 5000 });
                   $scope.loadConsignmentBookings();
                   // $(#).('hide')

               }, function errorCallback(response) {
                   Notification.error({ message: response.message, delay: 5000 });
               });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    /* BEGIN CONSIGNMNT PAYMENT */
    $scope.TotalFreight = 0;
    $scope.VendorCodeBookingpayments = '';
    $scope.GetVendorCode= function () {
        try {
            var cnName = $scope.consignmentBooking.ConsignorID ;
            var vendorCon = filterFilter($scope.Vendors, { VendorID: cnName });
            $scope.VendorCodeBookingpayments = vendorCon[0].VendorCode;
        }
        catch (ex) {
            throw ex;
        }
    }
    $scope.bankPaymentTypes = [];

    $scope.GetbankPaymentTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             pmtTypes.push(response.data[i]);
         }
         $scope.bankPaymentTypes = pmtTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.BookingPayment = {
        BookingPaymentID: '',
        ConsignmentID: '',
        GoodsDescription: '',
        InsuranceFlag: '',
        DocketCharges: '',
        FOV: '',
        DoorCollection: '',
        Octroicharges: '',
        DoorDelivery: 0,
        OctroiSurcharges: '',
        LabourCharges: '',
        OtherCharges: '',
        TotalAmount: '',
        TaxFlag: '',
        ServiceTaxID: '',
        RoadPermitType :'',
        RoadPermitAttached: '',
        Remark: '',
        GrandTotal: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        Status: '',
        PaymentTypeID: '',
        BankName: '',
        BranchName: '',
        IFSCCode: '',
        PanNo: '',
    };

    $scope.paymentTaxes = [];
  
    $scope.GetTaxes = function () {

        //$scope.activetrainingOpportunities = '';
        var taxes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTaxes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             taxes.push(response.data[i]);
         }
         $scope.paymentTaxes = taxes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.GetTotalFreight = function (consignmentID) {

        $http({
            method: 'GET',
            url: '/Operations/GetTotalFreight',
            params: { 'consignmentID': consignmentID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
        
         $scope.TotalFreight = response.data;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.GoodsDesc = [];
     $scope.GetGoods = function () {
        var goods = [];
        $http({
            method: 'GET',
            url: '/Operations/GetGoods',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             goods.push(response.data[i]);
         }
         $scope.GoodsDesc = goods;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
   
    $scope.clearBookingPayments = function () {

        $scope.BookingPayment = {
            BookingPaymentID: '',
            ConsignmentID: '',
            GoodsDescription: '',
            InsuranceFlag: '',
            DocketCharges: '',
            FOV: '',
            DoorCollection: 0,
            Octroicharges: '',
            DoorDelivery: '',
            OctroiSurcharges: '',
            LabourCharges: '',
            OtherCharges: '',
            TotalAmount: '',
            TaxFlag: '',
            ServiceTaxID: '',
            RoadPermitType: '',
            RoadPermitAttached: '',
            Remark: '',
            GrandTotal: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            Status: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            IFSCCode: '',
            PanNo: '',
        };


    }
    $scope.closeConsignmentModal = function()
    {
        $scope.clearBookingPayments();
        $scope.clearBookings();
        $('#ConsignmentModal').modal('hide');
    }
    $scope.TaxApplied = function () {
        if ($scope.BookingPayment.TaxFlag == "true" )
        {
            $scope.GetTaxes();
        }
        else
        {
            $scope.paymentTaxes = [];
        }
        $scope.CalculateGrandTotal();
    }
    $scope.CalculateGrandTotal = function()
    {
        
        var grandTotal =  $scope.BookingPayment.TotalAmount;
        var totalAmt = $scope.BookingPayment.TotalAmount;
        for (var i = 0; i < $scope.paymentTaxes.length; i++) {
            var taxVal = (totalAmt * $scope.paymentTaxes[i].TaxPercent) / 100;
            grandTotal = grandTotal + taxVal;
        }
        $scope.BookingPayment.GrandTotal = grandTotal;
    }
    $scope.CalculateTotalAmount = function () {

        var dockCharges = (($scope.BookingPayment.DocketCharges == '') ? 0 : $scope.BookingPayment.DocketCharges);
        var fovVal = (($scope.BookingPayment.FOV == '') ?0 :$scope.BookingPayment.FOV);
        var dcCharge = (($scope.BookingPayment.DoorCollection == '') ? 0 : $scope.BookingPayment.DoorCollection);
        var ddCharge = (($scope.BookingPayment.DoorDelivery == '') ? 0 : $scope.BookingPayment.DoorDelivery);
        var octCharge = (($scope.BookingPayment.Octroicharges == '')? 0 : $scope.BookingPayment.Octroicharges);
        var labcharge =(( $scope.BookingPayment.LabourCharges == '') ? 0 : $scope.BookingPayment.LabourCharges);
        var octSurcharg = (($scope.BookingPayment.OctroiSurcharges == '') ? 0 : $scope.BookingPayment.OctroiSurcharges);
        var othCharges = (($scope.BookingPayment.OtherCharges == '') ? 0 : $scope.BookingPayment.OtherCharges);

        
        var totalAmt = eval($scope.TotalFreight) +  eval(dockCharges ) + eval(fovVal) + eval( dcCharge ) + eval(ddCharge)
        + eval(octCharge )+ eval( labcharge ) + eval(octSurcharg )+ eval(othCharges);
        $scope.BookingPayment.TotalAmount = totalAmt;
        $scope.CalculateGrandTotal();
    }
    $scope.GetBookingPaymentByID = function (consignmentid) {

        var payment = [];
        $http({
            method: 'GET',
            params: { 'consignmentID': consignmentid, },
            url: '/Operations/GetConsignmentBookingPaymentByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         if (response.data.length > 0) {
             $scope.BookingPayment = {
                 BookingPaymentID: response.data[0].BookingPaymentID,
                 ConsignmentID: response.data[0].ConsignmentID,
                 GoodsDescription: response.data[0].GoodsDescription,
                 InsuranceFlag: response.data[0].InsuranceFlag,
                 DocketCharges: response.data[0].DocketCharges,
                 FOV: response.data[0].FOV,
                 DoorCollection: response.data[0].DoorCollection,
                 Octroicharges: response.data[0].Octroicharges,
                 DoorDelivery: response.data[0].DoorDelivery,
                 OctroiSurcharges: response.data[0].OctroiSurcharges,
                 LabourCharges: response.data[0].LabourCharges,
                 OtherCharges: response.data[0].OtherCharges,
                 TotalAmount: response.data[0].TotalAmount,
                 TaxFlag: response.data[0].TaxFlag,
                 ServiceTaxID: response.data[0].ServiceTaxID,
                 RoadPermitType: response.data[0].RoadPermitType,
                 RoadPermitAttached: response.data[0].RoadPermitAttached,
                 Remark: response.data[0].Remark,
                 GrandTotal: response.data[0].GrandTotal,
                 CreatedBy: response.data[0].CreatedBy,
                 Created: response.data[0].Created,
                 ModifiedBy: response.data[0].ModifiedBy,
                 Modified: response.data[0].Modified,
                 //PONo: response.data[0].PONo,
                 Status: response.data[0].Status,
                 PaymentTypeID: response.data[0].PaymentTypeID,
                 BankName: response.data[0].BankName,
                 BranchName: response.data[0].BranchName,
                 IFSCCode: response.data[0].IFSCCode,
                 PanNo: response.data[0].PanNo,
             };
             $scope.CalculateTotalAmount();
         }


     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.GetInvoiceTotal = function (consignmentID)
    {
        

            $http({
                method: 'GET',
                url: '/Operations/GetInvoiceTotal',
                params: { 'consignmentID': consignmentID },
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
         .then(function successCallback(response) {

             var invoiceTotal = response.data;
             $scope.BookingPayment.FOV = (invoiceTotal * 0.2)/100
         },
         function errorCallback(response) {
             alert(response);
         });
        
    }
    $scope.CalculateFOV = function()
    {
        if ($scope.BookingPayment.InsuranceFlag == "true" )
        {
            $scope.GetInvoiceTotal($scope.BookingPayment.ConsignmentID);
        }
        else
        {
            $scope.BookingPayment.FOV = 0;
        }
    }
    $scope.ApplyDoorDelivery = false;
      $scope.SelectBookingPayments = function () {
          if ($scope.consignmentBooking.ConsignmentID == '') {
              Notification.error({ message: "You have not saved Consignment Booking ,Payment can not be added before booking done.", delay: 5000 });
          }
          else {
              if ($scope.BookingPayment.ConsignmentID == '') {
                  consignmentID = $scope.consignmentBooking.ConsignmentID;
                  $scope.clearBookingPayments();
                  var deliveryTypeID = $scope.consignmentBooking.DeliveryTypeID;
                  var filterType = filterFilter($scope.DeliveryTypes, { DeliveryTypeID: deliveryTypeID });
                  if (filterType != null)
                  {
                      if (filterType[0].DeliveryType == 'Door')
                      {
                          $scope.ApplyDoorDelivery = true ;
                      }
                      else
                      {
                          $scope.ApplyDoorDelivery = false;
                      }
                  }
                  $scope.GetGoods();
                  $scope.GetbankPaymentTypes();
                  $scope.GetTaxes();
                  $scope.VendorCodeBookingpayments = '';
                  $scope.GetTotalFreight(consignmentID);
                  $scope.GetBookingPaymentByID(consignmentID)
                  $scope.BookingPayment.ConsignmentID = consignmentID;
                  $scope.GetVendorCode();
              }
          }
         
    }

    $scope.DeleteBookingPayments = function (paymentID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteConsignmentBookingPayment',
                data: paymentID,  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
             
          }, function errorCallback(response) {
              ////alert(response.data);
          });
        }
    }
    $scope.SaveBookingPayments = function () {

        var isValidConsignment = true;
        if ($scope.BookingPayment.ConsignmentID == '')
        {
            Notification.error({ message: "Consignment booking is not added !", delay: 5000 });
            isValidConsignment = false;
        }
        
        if ($scope.BookingPayment.InsuranceFlag == '')
        {
            $scope.BookingPayment.InsuranceFlag = false;
        }
        if ($scope.BookingPayment.RoadPermitAttached == '') {
            $scope.BookingPayment.RoadPermitAttached = false;
        }
            if ($scope.BookingPayment.BookingPaymentID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddConsignmentPayment',
                    data: JSON.stringify($scope.BookingPayment),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                // $scope.clearBookings();
                 $('#ConsignmentModal').modal('hide');

                 Notification.success({ message: "Consignment Booking Payment saved successfully.", delay: 5000 });
                 //$scope.loadConsignmentBookings();
                 // $(#).('hide')

             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateConsignmentPayment',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.BookingPayment),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
               .then(function successCallback(response) {
                   console.log(response);
                   //$scope.clearBookings();
                   //$('#ConsignmentModal').modal('hide');

                   Notification.success({ message: "Consignment Booking Payment saved successfully.", delay: 5000 });
                   //$scope.loadConsignmentBookings();
                   // $(#).('hide')

               }, function errorCallback(response) {
                   Notification.error({ message: response.message, delay: 5000 });
               });
            }

        
    }
    /* END CONSIGNMENT PAYMENT */


    /* Begin Party Invoice*/
    $scope.ConsignmentPartyInvoice = [];
        /*{
        PartyInvoiceID: '',
        ConsignmentID: '',
        InvoiceNo: '',
        InvoiceDate: '',
        InvoiceQuantity: '',
        InvoiceValue: '',
        PartNo: '',
        ASNNo: '',
        Article: '',
        PackagingID: '',
        ActualWeight: '',
        ChargeWeight: '',
        RatePerKG: '',
        BasicFreight: '',
        LxWxH: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        Status: '',
    };*/
    $scope.AddInvoiceRow = function()
    {
        $scope.ConsignmentPartyInvoice.push({
            PartyInvoiceID: '',
            ConsignmentID: $scope.consignmentBooking.ConsignmentID,
            InvoiceNo: '',
            InvoiceDate: '',
            InvoiceQuantity: '',
            InvoiceValue: '',
            PartNo: '',
            ASNNo: '',
            Article: '',
            PackagingID: '',
            ActualWeight: '',
            ChargeWeight: '',
            RatePerKG: '',
            BasicFreight: '',
            ChargeType :'',
            LxWxH: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            Status: '',
            PONo :''
        });

    }
    $scope.clearInvoice = function()
    {
        $scope.ConsignmentPartyInvoice = [];
        /*
        $scope.ConsignmentPartyInvoice = {
            PartyInvoiceID: '',
            ConsignmentID: $scope.consignmentBooking.ConsignmentID,
            InvoiceNo: '',
            InvoiceDate: '',
            InvoiceQuantity: '',
            InvoiceValue: '',
            PartNo: '',
            ASNNo: '',
            Article: '',
            PackagingID: '',
            ActualWeight: '',
            ChargeWeight: '',
            RatePerKG: '',
            BasicFreight: '',
            LxWxH: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            Status: '',
        };*/

    }

    
    $('#txtInvoiceDate').datepicker({
        format: 'dd/mm/yyyy',
        //startDate: '0d',
         endDate: '0d',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtInvoiceDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $scope.ChargeTypes = [];
    $scope.GetChargeTypes= function()
    {
        //$scope.activetrainingOpportunities = '';
        var chrgTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetChargeTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             chrgTypes.push(response.data[i]);
         }
         $scope.ChargeTypes = chrgTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.activePartyInvoices = [];
    $scope.activePartyInvoicesCollection = [];
    $scope.showAddInvoicePanel = false;
    $scope.showPartyInvoiceModal = function()
    {
        if ($scope.consignmentBooking.ConsignmentID != '') {
            $scope.GetPackagings();
            var consignmentID = $scope.consignmentBooking.ConsignmentID;
            $scope.clearInvoice();
            $scope.AddInvoiceRow();
            $scope.GetChargeTypes();
            $scope.ConsignmentPartyInvoice[0].ConsignmentID = consignmentID;
            
            $scope.loadPartyInvoice(consignmentID);
            $scope.showAddInvoicePanel = false;
            $('#PartyInvoiceModal').modal('show');
        }
        else
        {
            Notification.error({ message : 'Please add consignment booking for party Invoices', delay: 5000 });
        }
    }
    $scope.AddInvoice = function()
    {
        $scope.showAddInvoicePanel = true ;
    }
    $scope.loadPartyInvoice= function (consignmentID) {

        $scope.activePartyInvoices = '';
        var invoices = [];
        $http({
            method: 'GET',
            params: { 'consignmentID': consignmentID, },
            url: '/Operations/GetConsignmentPartyInvoice',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
            // response.data[i].InvoiceDate =$scope.FormatDate(response.data[i].InvoiceDate );
             invoices.push(response.data[i]);
         }
         $scope.activePartyInvoices = invoices;
         $scope.activePartyInvoicesCollection = invoices;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.DeletePartyInvoice= function (partyInvoiceID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteConsignmentPartyInvoice',
                params: {'partyInvoiceID' : partyInvoiceID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {

          }, function errorCallback(response) {
              var e = response;
              ////alert(response.data);
          });
        }
    }
    $scope.CalculateBasicFreight = function(index)
    {
        var chargetypeSelected = $scope.ConsignmentPartyInvoice[index].ChargeType;
        var chargeType = filterFilter($scope.ChargeTypes, { ChargeTypeID: chargetypeSelected });
        var basicFreight =0;
        if (chargeType.length > 0)
        {
            var chargeCode = chargeType[0].ChargeType;
            switch (chargeCode)
            {
                case 'Per Kg':
                    {
                        var chargeWt = 0;
                        if ($scope.ConsignmentPartyInvoice[index].ChargeWeight != '')
                            chargeWt = $scope.ConsignmentPartyInvoice[index].ChargeWeight;
                        var rateperKG = 0;
                        if ($scope.ConsignmentPartyInvoice[index].RatePerKG != '')
                            rateperKG = $scope.ConsignmentPartyInvoice[index].RatePerKG

                         basicFreight = chargeWt * rateperKG;
                        break;
                    }
                case 'Article':
                    {
                        
                        var noOfArticles = 0;
                        if ($scope.ConsignmentPartyInvoice[index].Article != '')
                            noOfArticles = $scope.ConsignmentPartyInvoice[index].Article;

                        if ($scope.ConsignmentPartyInvoice[index].RatePerKG != '')
                            rateperKG = $scope.ConsignmentPartyInvoice[index].RatePerKG

                        basicFreight = noOfArticles * rateperKG;
                        break;
                    }
                case 'CFT':
                    {
                        var noOfArticles = 0;
                        if ($scope.ConsignmentPartyInvoice[index].Article != '')
                            noOfArticles = $scope.ConsignmentPartyInvoice[index].Article;
                        var LWH = 0;
                        if ($scope.ConsignmentPartyInvoice[index].LxWxH != '')
                            LWH = $scope.ConsignmentPartyInvoice[index].LxWxH

                        basicFreight = noOfArticles * (LWH/1728 ) * 12;
                        break;
                    }

            }
        }
      
        $scope.ConsignmentPartyInvoice[index].BasicFreight = basicFreight;
    }
    $scope.closeInvoiceModal = function()
    {
        $scope.clearInvoice();
        $('#PartyInvoiceModal').modal('hide');
    }
    $scope.CancelInvoiceSave = function (idx)
    {
        if ($scope.ConsignmentPartyInvoice.length > 1) {
            $scope.ConsignmentPartyInvoice.splice(idx, 1);
        }
        else {
            Notification.error({ message: 'You must add atleast one row !', delay: 5000 });
            ////alert("You must add atleast one row to procure !");
        }
    }
    $scope.SavePartyInvoices= function (index) {

        var isValidConsignment = true;
        if ($scope.ConsignmentPartyInvoice[index].ConsignmentID == '') {
            Notification.error({ message: "Consignment booking is not added !", delay: 5000 });
            isValidConsignment = false;
        }
        var isValidInput = true;
        if (($scope.ConsignmentPartyInvoice[index].InvoiceNo == '') || ($scope.ConsignmentPartyInvoice[index].InvoiceDate == '')
            || ($scope.ConsignmentPartyInvoice[index].InvoiceQuantity == '') || ($scope.ConsignmentPartyInvoice[index].InvoiceValue == '')
            || ($scope.ConsignmentPartyInvoice[index].Article == '') || ($scope.ConsignmentPartyInvoice[index].PackagingID == '')
            || ($scope.ConsignmentPartyInvoice[index].ActualWeight == '') || ($scope.ConsignmentPartyInvoice[index].ChargeWeight == '')
            )
        {
            isValidInput = false ;
        }
       

        if (isValidInput)
        {
            $scope.CalculateBasicFreight(index);
            $http({
                method: 'POST',
                url: '/Operations/AddConsignmentPartyInvoice',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.ConsignmentPartyInvoice[index]),
                    
            })
         .then(function successCallback(response) {
             console.log(response);
             // $scope.clearBookings();
               

             Notification.success({ message: "Consignment Party Invoices saved successfully.", delay: 5000 });
                 
             $scope.loadPartyInvoice($scope.ConsignmentPartyInvoice[index].ConsignmentID);
             $scope.clearInvoice();
             $scope.AddInvoiceRow();
             $scope.showAddInvoicePanel = false;
         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
           
        }
        else
        {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }

    }
    /*End Party Invoice */
}]);