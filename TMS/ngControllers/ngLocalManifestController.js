﻿App.controller('LocalManifest_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {

    $("#select_all").change(function () {  //"select all" change 
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function () { //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            $scope.activeConsignmentBooking[i].checkSelected = true;
        }

    });

    $('.checkbox').change(function () { //".checkbox" change 
        if (this.checked == false) { //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
    /**ADD HIRED VEHICLES **/
    $scope.VehicleMaster = {
        VehicleName: '',
        VehicleNo: '',
        VehicleType: '',
        RegistrationDate: '',
        FitnessExpDate: '',
        InsuranceExpDate: '',
        PUCExpDate: '',
        EngineNo: '',
        ChasisNo: '',
        OwnerType: '',
        BrokerName: '',
        VendorName: '',
        loadingCapacity: '',
        MaxloadingCapacity: '',
        isHiredVehicle: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    }
    $scope.closeVehicelModal = function () {
        $scope.VehicleMaster = {
            VehicleName: '',
            VehicleNo: '',
            VehicleType: '',
            RegistrationDate: '',
            FitnessExpDate: '',
            InsuranceExpDate: '',
            PUCExpDate: '',
            EngineNo: '',
            ChasisNo: '',
            OwnerType: '',
            BrokerName: '',
            VendorName: '',
            loadingCapacity: '',
            MaxloadingCapacity: '',
            isHiredVehicle: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        }
        $('#AddHiredVehicleModal').modal('hide');
    }
    $scope.showVehicleModal = function ()
    { $('#AddHiredVehicleModal').modal('show'); }
    $scope.AddVehicle = function () {

        if ($scope.frmVehicle.$valid) {
            var isHired = true;
            $scope.VehicleMaster.isHiredVehicle = isHired;
            $http({
                method: 'POST',
                url: '/Operations/AddVehicle',
                data: JSON.stringify($scope.VehicleMaster),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.closeVehicelModal();

                 Notification.success({ message: "Hired Vehicle saved successfully.", delay: 5000 });

                 $scope.GetVehicles(isHired);

             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
        }



        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    /**END ADD HIRED VEHICLES **/
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.currentTime = $filter('date')(new Date(), 'hh:mm:ss');
    $('#txtRegistrationDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtRegistrationDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtEstDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtEstDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $('#txtFitnessExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtFitnessExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtInsuranceExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtInsuranceExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtPUCExpDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-1d',
        // endDate: '0d',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtPUCExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $scope.LocalVehicleHire = {
       LocalVehicleHireID: '',
        BranchID: '',
        HireDate: $scope.currentDate,
        TruckBookingType: '',
        VehicleID: '',
        VendorName: '',
        VendorType: '',
        BookingAmount: '',
        AdvancePayment: '',
        BalanceAmount: '',
        PaymentTypeID: '',
        BankName: '',
        BranchName: '',
        IFSCCode: '',
        PanNo: '',
        Remarks: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    };


    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }



    $scope.activeVehicleHires = [];
    $scope.activeVehicleHireCollection = [];
    $scope.PaymentTypes = [];
    $scope.Branches = [];
    $scope.GetBranches = function () {

        //$scope.activetrainingOpportunities = '';
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             brnches.push(response.data[i]);
         }
         $scope.Branches = brnches;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }



    $scope.GetPaymentTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             pmtTypes.push(response.data[i]);
         }
         $scope.PaymentTypes = pmtTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.BookingTypes = [];
    $scope.GetTruckBookingTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var bkTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTruckBookingTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             bkTypes.push(response.data[i]);
         }
         $scope.BookingTypes = bkTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.VendorTypes = [];
    $scope.GetVendorTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var venTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendorTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             venTypes.push(response.data[i]);
         }
         $scope.VendorTypes = venTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.Vehicles = [];
    $scope.GetVehicles = function (isHired) {


        var vehList = [];
        $http({
            method: 'GET',
            params: { 'isHired': isHired, },
            url: '/Operations/GetVehicles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             vehList.push(response.data[i]);
         }
         $scope.Vehicles = vehList;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    //$scope.showVehicleDropdown = false;
    //$scope.showVehicleTextInput = true;
    $scope.EnableVendor = false;
    $scope.HiredandNew = false;
    $scope.EnableVehicle = function () {
        var booktypeSelected = $scope.LocalVehicleHire.TruckBookingType;
        var bookType = filterFilter($scope.BookingTypes, { BookingTypeID: booktypeSelected });
        var isHired = false;
        var vendorType = "";
        var vendortypeSelected = $scope.LocalVehicleHire.VendorType;
        var vendorType = filterFilter($scope.VendorTypes, { VendorTypeID: vendortypeSelected });

        if (bookType.length > 0) {
            if (bookType[0].BookingType == 'Own') {
                isHired = false;
                $scope.EnableVendor = false;
                $scope.HiredandNew = false;
            }
            else {
                if (vendorType[0].VendorType == 'New') {
                    $scope.HiredandNew = true;
                   
                }
                isHired = true;
                $scope.EnableVendor = true;
            }

            $scope.GetVehicles(isHired);
        }
    }
    $scope.loadVehicleHirePayments = function () {

        $scope.activeVehicleHires = [];
        var hirePayments = [];
        $http({
            method: 'GET',
            //  params: { 'branchID': branchID, },
            url: '/Operations/GetLocalVehicleHirePayments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             hirePayments.push(response.data[i]);
         }
         $scope.activeVehicleHires = hirePayments;
         $scope.activeVehicleHireCollection = hirePayments;
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.clearVehicleHire = function () {
        $scope.LocalVehicleHire = {
            LocalVehicleHireID: '',
            BranchID: '',
            HireDate: $scope.currentDate,
            TruckBookingType: '',
            VehicleID: '',
            VendorName: '',
            VendorType: '',
            BookingAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            IFSCCode: '',
            PanNo: '',
            Remarks: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        };


    }

    $scope.GetVehicleHirepaymentByID = function (VehicleHireID) {

        var hirepayments = [];
        $http({
            method: 'GET',
            params: { 'vehicleHireID': VehicleHireID },
            url: '/Operations/GetLocalVehicleHirePaymentByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             hirepayments.push(response.data[i]);
         }


         $scope.LocalVehicleHire.LocalVehicleHireID = hirepayments[0].LocalVehicleHireID;
         $scope.LocalVehicleHire.BranchID = hirepayments[0].BranchID;
         $scope.LocalVehicleHire.TruckBookingType = hirepayments[0].TruckBookingType;
         $scope.LocalVehicleHire.VendorName = hirepayments[0].VendorName;
         $scope.LocalVehicleHire.VendorType = hirepayments[0].VendorType;
         $scope.LocalVehicleHire.BookingAmount = hirepayments[0].BookingAmount;
         $scope.LocalVehicleHire.AdvancePayment = hirepayments[0].AdvancePayment;
         $scope.LocalVehicleHire.BalanceAmount = hirepayments[0].BalanceAmount;
         $scope.LocalVehicleHire.PaymentTypeID = hirepayments[0].PaymentTypeID;
         $scope.LocalVehicleHire.BankName = hirepayments[0].BankName;
         $scope.LocalVehicleHire.BranchName = hirepayments[0].BranchName;
         $scope.LocalVehicleHire.IFSCCode = hirepayments[0].IFSCCode;
         $scope.LocalVehicleHire.PanNo = hirepayments[0].PanNo;
         $scope.LocalVehicleHire.Remarks = hirepayments[0].Remarks;
         $scope.LocalVehicleHire.CreatedBy = hirepayments[0].CreatedBy;
         $scope.LocalVehicleHire.Created = hirepayments[0].Created;
         $scope.LocalVehicleHire.ModifiedBy = hirepayments[0].ModifiedBy;
         $scope.LocalVehicleHire.Modified = hirepayments[0].Modified;

         $scope.EnableVehicle();
         $scope.LocalVehicleHire.VehicleID = hirepayments[0].VehicleID;
         $scope.GetVehicleDetails();
         $scope.LocalVehicleHire.HireDate = $filter('date')($scope.FormatDate($scope.LocalVehicleHire.HireDate), 'dd/MM/yyyy');

     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.closeVehicleHire = function () {
        $scope.clearVehicleHire();
        $('#LocalVehicleHireModal').modal('hide');
    }
    $scope.AddVehicleHirepayment = function () {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetVendorTypes();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();
        $('#LocalVehicleHireModal').modal('show');
    }


    $scope.EditVehicleHirepayment = function (LocalVehicleHireID) {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetVendorTypes();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();

        $scope.GetVehicleHirepaymentByID(LocalVehicleHireID);

        $('#LocalVehicleHireModal').modal('show');

    }
    $scope.RegistrationDate = "";
    $scope.FitnessExpDate = "";
    $scope.InsuranceExpDate = "";
    $scope.PUCExpDate = "";
    $scope.GetVehicleDetails = function () {
        var vehID = $scope.LocalVehicleHire.VehicleID;
        var vehDetails = filterFilter($scope.Vehicles, { VehicleID: vehID });
        if (vehDetails.length > 0) {
            $scope.RegistrationDate = $filter('date')($scope.FormatDate(vehDetails[0].RegistrationDate), 'dd/MM/yyyy');
            $scope.FitnessExpDate = $filter('date')($scope.FormatDate(vehDetails[0].FitnessExpDate), 'dd/MM/yyyy');
            $scope.InsuranceExpDate = $filter('date')($scope.FormatDate(vehDetails[0].InsuranceExpDate), 'dd/MM/yyyy');
            $scope.PUCExpDate = $filter('date')($scope.FormatDate(vehDetails[0].PUCExpDate), 'dd/MM/yyyy');
        }
    }
    $scope.calculateBalanceAmount = function () {
        var bookingAmt = 0;
        if ($scope.LocalVehicleHire.BookingAmount != '')
            bookingAmt = parseFloat($scope.LocalVehicleHire.BookingAmount);
        var advPayment = 0;
        if ($scope.LocalVehicleHire.AdvancePayment != '')
            advPayment = parseFloat($scope.LocalVehicleHire.AdvancePayment);
        if (advPayment > bookingAmt) {
            Notification.error({ message: 'Advance payment can not be greater than booking amount !', delay: 5000 });
        }
        var balanceAmt = eval(bookingAmt - advPayment);
        $scope.LocalVehicleHire.BalanceAmount = balanceAmt;

    }
    $scope.DeleteVehicleHirePayment = function (vehicleHireID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteLocalVehicleHire',
                params: { 'vehicleHireID': vehicleHireID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
              Notification.success({ message: "Local Vehicle Hire payment deleted successfully.", delay: 5000 });
              $scope.loadVehicleHirePayments();
          }, function errorCallback(response) {
              var e = response.message;
          });
        }
    }
    $scope.SaveVehicleHire = function () {

        if ($scope.frmVehHire.$valid) {
            if ($scope.LocalVehicleHire.LocalVehicleHireID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddLocalVehicleHirePayment',
                    data: JSON.stringify($scope.LocalVehicleHire),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.clearVehicleHire();
                 $('#LocalVehicleHireModal').modal('hide');
                 Notification.success({ message: "Local Vehicle Hire payment saved successfully.", delay: 5000 });

                 $scope.loadVehicleHirePayments();

             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateLocalVehicleHirePayment',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.LocalVehicleHire),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
               .then(function successCallback(response) {
                   console.log(response);
                   $scope.clearVehicleHire();
                   $('#LocalVehicleHireModal').modal('hide');

                   Notification.success({ message: "Local Vehicle Hire payment updated successfully.", delay: 5000 });
                   $scope.loadVehicleHirePayments();
                   // $(#).('hide')

               }, function errorCallback(response) {
                   Notification.error({ message: response.message, delay: 5000 });
               });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    /* BEGIN Manifeast */
    $scope.ManifestSaved = false;
    $scope.VehicleManifest = {
        ManifestID: '',
        VehicleHireID: '',
        ManifestNo: '',
        VehicleID: '',
        DriverName: '',
        DriverMobileNo: '',
        LicenseNo: '',
        EstimatedDeliveryDate: '',
        ArrivalTime: '',
        DepartureTime: $scope.currentTime,
        FromLoc: '',
        ToLoc: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    };

    $scope.GetNextManifestNo = function () {

        var branchID = $('#hdnBranchID').attr("value");;

        $http({
            method: 'GET',
            params: { 'branchID': branchID },
            url: '/Operations/GetNextLocalManifestNo',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         $scope.VehicleManifest.ManifestNo = response.data;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.activeConsignmentBooking = [];
    $scope.activeConsignmentBookingCollection = [];
    $scope.GetConsignmentsForManifest = function () {

        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
            params: { 'manifestId': $scope.VehicleManifest.ManifestID, },
            url: '/Operations/GetConsignmentsForLocalManifest',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             bookings.push(response.data[i]);
             bookings[i].checkSelected = false;
         }
         $scope.activeConsignmentBooking = bookings;
         $scope.activeConsignmentBookingCollection = bookings;
         $scope.GetManifestConsignments();
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.loadVehicleManifest = function (vehicleHireItem) {


        $http({
            method: 'GET',
            params: { 'vehicleHireID': vehicleHireItem.LocalVehicleHireID, },
            url: '/Operations/GetLocalVehicleHireManifestByVehicleHireID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         if (response.data.length > 0) {
             $scope.VehicleManifest = {
                 ManifestID: response.data[0].ManifestID,
                 VehicleHireID: response.data[0].VehicleHireID,
                 ManifestNo: response.data[0].ManifestNo,
                 VehicleID: response.data[0].VehicleID,
                 DriverName: response.data[0].DriverName,
                 DriverMobileNo: response.data[0].DriverMobileNo,
                 LicenseNo: response.data[0].LicenseNo,
                 EstimatedDeliveryDate: $filter('date')($scope.FormatDate(response.data[0].EstimatedDeliveryDate), 'dd/MM/yyyy'),
                 ArrivalTime: response.data[0].ArrivalTime,
                 DepartureTime: response.data[0].DepartureTime,
                 FromLoc: response.data[0].FromLoc,
                 ToLoc: response.data[0].ToLoc,
                 CreatedBy: response.data[0].CreatedBy,
                 Created: $filter('date')($scope.FormatDate(response.data[0].Created), 'dd/MM/yyyy'),
                 ModifiedBy: response.data[0].ModifiedBy,
                 Modified: response.data[0].Modified,
             };
             $scope.ManifestSaved = true;
         }
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.clearManifest = function () {
        $scope.showManifestConsignments = false;
        $scope.ManifestSaved = false;
        $scope.ManifestVehicleNo = '';
        $scope.ManifestVehicleType = '';
        $scope.VehicleManifest = {
            ManifestID: '',
            VehicleHireID: '',
            ManifestNo: '',
            VehicleID: '',
            DriverName: '',
            DriverMobileNo: '',
            LicenseNo: '',
            EstimatedDeliveryDate: '',
            ArrivalTime: '',
            DepartureTime: '',
            FromLoc: '',
            ToLoc: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        };
    }

    $scope.ManifestVehicleNo = '';
    $scope.ManifestVehicleType = '';
    $scope.closeManifest = function () {

        $scope.clearManifest();
        $('#ManifestModal').modal('hide');
    }

    $scope.AddManifest = function (vehicleHireItem) {
        $scope.clearManifest();
        $scope.GetBranches();
       
        $scope.loadVehicleManifest(vehicleHireItem);
        $scope.VehicleManifest.DepartureTime = $scope.currentTime;
        $scope.GetSelectedVehicleForHire(vehicleHireItem.VehicleID);

        $scope.VehicleManifest.VehicleHireID = vehicleHireItem.LocalVehicleHireID;
        $scope.VehicleManifest.Created = $scope.currentDate;
        $('#ManifestModal').modal('show');
    }
    $scope.GetSelectedVehicleForHire = function (vehicleID) {

        $http({
            method: 'GET',
            params: { 'vehicleID': vehicleID, },
            url: '/Operations/GetVehicleByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    .then(function successCallback(response) {
        if (response.data.length > 0) {
            $scope.ManifestVehicleNo = response.data[0].VehicleNo;
        }


    },
     function errorCallback(response) {
         alert(response);
     });

    }

    $scope.DeleteManifest = function (manifestID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteLocalVehicleHireManifest',
                params: { 'manifestID': manifestID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
              Notification.success({ message: "Local Vehicle Manifest deleted successfully.", delay: 5000 });
              $scope.loadVehicleHirePayments();
          }, function errorCallback(response) {
              var e = response.message;
          });
        }
    }
    $scope.SaveManifest = function () {

        if ($scope.frmManifest.$valid) {
            if ($scope.VehicleManifest.ManifestID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddLocalVehicleHireManifest',
                    data: JSON.stringify($scope.VehicleManifest),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.clearManifest();
                 // $('#ManifestModal').modal('hide');
                 Notification.success({ message: "Local Vehicle Manifest saved successfully.", delay: 5000 });
                 var manifestID = response.data;

                 $scope.VehicleManifest.ManifestID = manifestID;

                 $scope.ManifestSaved = true;

             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateLocalVehicleHireManifest',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.VehicleManifest),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
               .then(function successCallback(response) {
                   console.log(response);
                   $scope.clearManifest();
                   $('#ManifestModal').modal('hide');
                   $scope.ManifestSaved = true;
                   Notification.success({ message: "Local Vehicle Manifest updated successfully.", delay: 5000 });
                   $scope.loadConsignmentBookings();
                   // $(#).('hide')

               }, function errorCallback(response) {
                   Notification.error({ message: response.message, delay: 5000 });
               });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }


    //Manifest Consignments
    $scope.ManifestConsignment = {
        ManifestConsignmentID: '',
        ManifestID: '',
        ConsignmentID: '',
        CreatedBy: '',
        Created: ''
    }
    $scope.ManifestConsignment = [];
    $scope.ShowConsignment = function () {
        $scope.GetConsignmentsForManifest();
        //$scope.GetManifestConsignments();
        $scope.showManifestConsignments = true;
    }
    $scope.CancelConsignment = function () {

        $scope.activeConsignmentBooking = [];
        $scope.showManifestConsignments = false;
    }

    $scope.GetManifestConsignments = function () {

        $http({
            method: 'GET',
            params: { 'manifestID': $scope.VehicleManifest.ManifestID },
            url: '/Operations/GetLocalManifestConsignments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             //manifestCnsgnment.push(response.data[i]);

             for (j = 0; j < $scope.activeConsignmentBooking.length; j++) {
                 if ($scope.activeConsignmentBooking[j].ConsignmentID == response.data[i].ConsignmentID) {
                     $scope.activeConsignmentBooking[j].checkSelected = true;
                     break;
                 }
             }
         }
         $scope.ManifestConsignment = manifestCnsgnment;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.AddManifestConsignments = function () {

        var newManifestConsignments = [];
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            //var traingOpp  = [];
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {

                var mfConsignment = {
                    ManifestConsignmentID: '',
                    ManifestID: $scope.VehicleManifest.ManifestID,
                    ConsignmentID: $scope.activeConsignmentBooking[i].ConsignmentID,
                    CreatedBy: '',
                    Created: ''
                }
                newManifestConsignments.push(mfConsignment);
                // traingOpp.push(activetrainingPlanOpps[i]);
            }


        }

        $http({
            method: 'POST',
            url: '/Operations/AddLocalManifestConsignment',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(newManifestConsignments)

        })
         .then(function successCallback(response) {
             console.log(response);


             Notification.success({ message: "Local Manifest consignments saved successfully.", delay: 5000 });

         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
        //}
    }

    /*End Manifest*/


}]);