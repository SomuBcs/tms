﻿App.controller('Manifest_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {

 $("#select_all").change(function () {  //"select all" change 
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function () { //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            $scope.activeConsignmentBooking[i].checkSelected = true;
            }
    
    });

    $('.checkbox').change(function () { //".checkbox" change 
        if (this.checked == false) { //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
   /**ADD HIRED VEHICLES **/
    $scope.VehicleMaster = {
        VehicleName: '',
        VehicleNo: '',
        VehicleType: '',
        RegistrationDate: '',
        FitnessExpDate: '',
        InsuranceExpDate: '',
        PUCExpDate: '',
        EngineNo: '',
        ChasisNo: '',
        OwnerType: '',
        BrokerName: '',
        VendorName: '',
        loadingCapacity: '',
        MaxloadingCapacity: '',
        isHiredVehicle: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    }
    $scope.closeVehicelModal = function()
    {
        $scope.VehicleMaster = {
            VehicleName: '',
            VehicleNo: '',
            VehicleType: '',
            RegistrationDate: '',
            FitnessExpDate: '',
            InsuranceExpDate: '',
            PUCExpDate: '',
            EngineNo: '',
            ChasisNo: '',
            OwnerType: '',
            BrokerName: '',
            VendorName: '',
            loadingCapacity: '',
            MaxloadingCapacity: '',
            isHiredVehicle: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        }
        $('#AddHiredVehicleModal').modal('hide');
    }
    $scope.showVehicleModal = function ()
    { $('#AddHiredVehicleModal').modal('show'); }
    $scope.AddVehicle = function () {

        if ($scope.frmVehicle.$valid) {
            var isHired = true;
            $scope.VehicleMaster.isHiredVehicle = isHired;
            $http({
                    method: 'POST',
                    url: '/Operations/AddVehicle',
                    data: JSON.stringify($scope.VehicleMaster),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.closeVehicelModal();
                
                 Notification.success({ message: "Hired Vehicle saved successfully.", delay: 5000 });
                 
                 $scope.GetVehicles(isHired);

             }, function errorCallback(response) {
                 Notification.error({ message: response.message, delay: 5000 });
             });
            }
      

        
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    /**END ADD HIRED VEHICLES **/
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.currentTime  = $filter('date')(new Date(), 'hh:mm:ss');
    $('#txtRegistrationDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtRegistrationDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    
    $('#txtEstDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtEstDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $('#txtFitnessExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtFitnessExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtInsuranceExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtInsuranceExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtPUCExpDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-1d',
        // endDate: '0d',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtPUCExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $scope.VehicleHire = {
        VehicleHireID: '',
        BranchID: '',
        HireDate: $scope.currentDate,
        TruckBookingType: '',
        VehicleID: '',
        VendorName: '',
        VendorType: '',
        BookingAmount: '',
        AdvancePayment: '',
        BalanceAmount: '',
        PaymentTypeID: '',
        BankName: '',
        BranchName: '',
        IFSCCode: '',
        PanNo: '',
        Remarks: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    };

  
    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }
  


    $scope.activeVehicleHires= [];
    $scope.activeVehicleHireCollection = [];
    $scope.PaymentTypes = [];
    $scope.Branches =[];
    $scope.GetBranches = function () {

        //$scope.activetrainingOpportunities = '';
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             brnches.push(response.data[i]);
         }
         $scope.Branches = brnches;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

   

    $scope.GetPaymentTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             pmtTypes.push(response.data[i]);
         }
         $scope.PaymentTypes = pmtTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.BookingTypes = [];
    $scope.GetTruckBookingTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var bkTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTruckBookingTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             bkTypes.push(response.data[i]);
         }
         $scope.BookingTypes = bkTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.VendorTypes = [];
    $scope.GetVendorTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var venTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendorTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             venTypes.push(response.data[i]);
         }
         $scope.VendorTypes = venTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.Vehicles = [];
    $scope.GetVehicles = function (isHired) {

        
        var vehList = [];
        $http({
            method: 'GET',
            params: { 'isHired': isHired, },
            url: '/Operations/GetVehicles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             vehList.push(response.data[i]);
         }
         $scope.Vehicles = vehList;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    //$scope.showVehicleDropdown = false;
    //$scope.showVehicleTextInput = true;
    $scope.EnableVendor = false;
    $scope.HiredandNew = false;
    $scope.EnableVehicle = function()
    {
        var booktypeSelected = $scope.VehicleHire.TruckBookingType; 
        var bookType = filterFilter($scope.BookingTypes, { BookingTypeID: booktypeSelected });
        var isHired = false;
        var vendorType = "";
        var vendortypeSelected = $scope.VehicleHire.VendorType;
        var vendorType = filterFilter($scope.VendorTypes, { VendorTypeID: vendortypeSelected });

        if (bookType.length > 0) {
            if (bookType[0].BookingType == 'Own')
            {
                isHired = false ;
                $scope.EnableVendor = false;
                $scope.HiredandNew = false;
            }
            else
            {
                if (vendorType[0].VendorType == 'New') {
                    $scope.HiredandNew = true;
                 
                }
                isHired = true;
                $scope.EnableVendor = true;
            }

            $scope.GetVehicles(isHired);
        }
    }
    $scope.loadVehicleHirePayments= function () {

        $scope.activeVehicleHires = [];
        var hirePayments = [];
        $http({
            method: 'GET',
            //  params: { 'branchID': branchID, },
            url: '/Operations/GetVehicleHirePayments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             hirePayments.push(response.data[i]);
         }
         $scope.activeVehicleHires = hirePayments;
         $scope.activeVehicleHireCollection = hirePayments;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

   
    $scope.clearVehicleHire = function () {
        $scope.VehicleHire = {
            VehicleHireID: '',
            BranchID: '',
            HireDate: $scope.currentDate,
            TruckBookingType: '',
            VehicleID: '',
            VendorName: '',
            VendorType: '',
            BookingAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            IFSCCode:'',
            PanNo: '',
            Remarks: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        };


    }

    $scope.GetVehicleHirepaymentByID = function (VehicleHireID) {

        var hirepayments = [];
        $http({
            method: 'GET',
            params: { 'vehicleHireID': VehicleHireID },
            url: '/Operations/GetVehicleHirePaymentByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             hirepayments.push(response.data[i]);
         }


         $scope.VehicleHire.VehicleHireID = hirepayments[0].vehicleHireID;
         $scope.VehicleHire.BranchID = hirepayments[0].BranchID;
         $scope.VehicleHire.TruckBookingType = hirepayments[0].TruckBookingType;
         $scope.VehicleHire.VendorName = hirepayments[0].VendorName;
         $scope.VehicleHire.VendorType = hirepayments[0].VendorType;
         $scope.VehicleHire.BookingAmount = hirepayments[0].BookingAmount;
         $scope.VehicleHire.AdvancePayment = hirepayments[0].AdvancePayment;
         $scope.VehicleHire.BalanceAmount = hirepayments[0].BalanceAmount;
         $scope.VehicleHire.PaymentTypeID = hirepayments[0].PaymentTypeID;
         $scope.VehicleHire.BankName = hirepayments[0].BankName;
         $scope.VehicleHire.BranchName = hirepayments[0].BranchName;
         $scope.VehicleHire.IFSCCode = hirepayments[0].IFSCCode;
         $scope.VehicleHire.PanNo = hirepayments[0].PanNo;
         $scope.VehicleHire.Remarks = hirepayments[0].Remarks;
         $scope.VehicleHire.CreatedBy = hirepayments[0].CreatedBy;
         $scope.VehicleHire.Created = hirepayments[0].Created;
         $scope.VehicleHire.ModifiedBy = hirepayments[0].ModifiedBy;
         $scope.VehicleHire.Modified = hirepayments[0].Modified;

         $scope.EnableVehicle();
         $scope.VehicleHire.VehicleID = hirepayments[0].VehicleID;
         $scope.GetVehicleDetails();
         $scope.VehicleHire.HireDate = $scope.FormatDate($scope.VehicleHire.HireDate);// $filter('date')($scope.FormatDate($scope.VehicleHire.HireDate),'dd/MM/yyyy');
        
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.closeVehicleHire = function()
    {
        $scope.clearVehicleHire();
        $('#VehicleHireModal').modal('hide');
    }
    $scope.AddVehicleHirepayment = function () {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetVendorTypes();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();
        $('#VehicleHireModal').modal('show');
    }


    $scope.EditVehicleHirepayment  = function (vehicleHireID) {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetVendorTypes();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();
        
        $scope.GetVehicleHirepaymentByID(vehicleHireID);
       
        $('#VehicleHireModal').modal('show');
        
    }
    $scope.RegistrationDate = "";
    $scope.FitnessExpDate = "";
    $scope.InsuranceExpDate = "";
    $scope.PUCExpDate = "";
    $scope.GetVehicleDetails = function()
    {
        var vehID = $scope.VehicleHire.VehicleID;
        var vehDetails = filterFilter($scope.Vehicles, { VehicleID: vehID });
        if (vehDetails.length > 0)
        {
            $scope.RegistrationDate = $filter('date')($scope.FormatDate(vehDetails[0].RegistrationDate), 'dd/MM/yyyy');
            $scope.FitnessExpDate = $filter('date')($scope.FormatDate(vehDetails[0].FitnessExpDate), 'dd/MM/yyyy');
            $scope.InsuranceExpDate =  $filter('date')($scope.FormatDate(vehDetails[0].InsuranceExpDate), 'dd/MM/yyyy');
            $scope.PUCExpDate = $filter('date')($scope.FormatDate(vehDetails[0].PUCExpDate), 'dd/MM/yyyy');
        }
    }
    $scope.calculateBalanceAmount = function()
    {
        var bookingAmt = 0;
        if ($scope.VehicleHire.BookingAmount != '')
            bookingAmt = parseFloat($scope.VehicleHire.BookingAmount);
        var advPayment = 0;
        if ($scope.VehicleHire.AdvancePayment != '')
            advPayment = parseFloat($scope.VehicleHire.AdvancePayment);
        if (advPayment > bookingAmt)
        {
            Notification.error({ message: 'Advance payment can not be greater than booking amount !', delay: 5000 });
        }
        var balanceAmt = eval(bookingAmt - advPayment);
        $scope.VehicleHire.BalanceAmount = balanceAmt;

    }
    $scope.DeleteVehicleHirePayment = function (vehicleHireID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteVehicleHire',
                params: { 'vehicleHireID': vehicleHireID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
              Notification.success({ message: "Vehicle Hire payment deleted successfully.", delay: 5000 });
              $scope.loadVehicleHirePayments();
          }, function errorCallback(response) {
              var e = response.message;
          });
        }
    }
    $scope.SaveVehicleHire= function () {

        if ($scope.frmVehHire.$valid) {
            if ($scope.VehicleHire.VehicleHireID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddVehicleHirePayment',
                    data: JSON.stringify($scope.VehicleHire),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.clearVehicleHire();
                 $('#VehicleHireModal').modal('hide');
                 Notification.success({ message: "Vehicle Hire payment saved successfully.", delay: 5000 });
                
                 $scope.loadVehicleHirePayments();

             }, function errorCallback(response) {
                 alert(JSON.stringify(response.statusText) );
                 // Notification.error({ message: response.data, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateVehicleHirePayment',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.VehicleHire),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
               .then(function successCallback(response) {
                   console.log(response);
                   $scope.clearVehicleHire();
                   $('#VehicleHireModal').modal('hide');

                   Notification.success({ message: "Vehicle Hire payment updated successfully.", delay: 5000 });
                   $scope.loadVehicleHirePayments();
                   // $(#).('hide')

               }, function errorCallback(response) {
                   Notification.error({ message: response.data, delay: 5000 });
               });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    /* BEGIN Manifeast */
    $scope.ManifestSaved = false;
    $scope.VehicleManifest = {
        ManifestID: '',
        VehicleHireID: '',
        ManifestNo: '',
        DriverName: '',
        DriverMobileNo: '',
        LicenseNo: '',
        EstimatedDeliveryDate: '',
        ArrivalTime: '',
        DepartureTime: $scope.currentTime,
        FromLoc: '',
        ToLoc: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        ArrivalDate: '',
        ManifestUnloaded: '',
        DeliveryStatus: '',
    };

    $scope.GetNextManifestNo = function () {

        var branchID = $('#hdnBranchID').attr("value");;

        $http({
            method: 'GET',
            params: { 'branchID': branchID },
            url: '/Operations/GetNextManifestNo',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         $scope.VehicleManifest.ManifestNo = response.data;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.activeConsignmentBooking = [];
    $scope.activeConsignmentBookingCollection = [];
    $scope.GetConsignmentsForManifest = function () {

        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
            params: { 'manifestId': $scope.VehicleManifest.ManifestID, },
            url: '/Operations/GetConsignmentsForManifest',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             bookings.push(response.data[i]);
             bookings[i].checkSelected = false;
         }
         $scope.activeConsignmentBooking = bookings;
         $scope.activeConsignmentBookingCollection = bookings;
         $scope.GetManifestConsignments();
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.loadVehicleManifest = function (vehicleHireItem) {

      
        $http({
            method: 'GET',
            params: { 'vehicleHireID': vehicleHireItem.VehicleHireID, },
            url: '/Operations/GetVehicleHireManifestByVehicleHireID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         if (response.data.length > 0) {
             
             $scope.VehicleManifest.ManifestID =  response.data[0].ManifestID;
             $scope.VehicleManifest.VehicleHireID =  response.data[0].VehicleHireID;
             $scope.VehicleManifest.ManifestNo =  response.data[0].ManifestNo;
             $scope.VehicleManifest.DriverName =  response.data[0].DriverName;
             $scope.VehicleManifest.DriverMobileNo =  response.data[0].DriverMobileNo;
             $scope.VehicleManifest.LicenseNo =  response.data[0].LicenseNo;
             $scope.VehicleManifest.EstimatedDeliveryDate =  $filter('date')($scope.FormatDate(response.data[0].EstimatedDeliveryDate), 'dd/MM/yyyy');
             $scope.VehicleManifest.ArrivalTime =  response.data[0].ArrivalTime;
             $scope.VehicleManifest.DepartureTime =  response.data[0].DepartureTime;
             $scope.VehicleManifest.FromLoc =  response.data[0].FromLoc;
             $scope.VehicleManifest.ToLoc =  response.data[0].ToLoc;
             $scope.VehicleManifest.CreatedBy =  response.data[0].CreatedBy;
             $scope.VehicleManifest.Created =  $filter('date')($scope.FormatDate(response.data[0].Created), 'dd/MM/yyyy');
             $scope.VehicleManifest.ModifiedBy =  response.data[0].ModifiedBy;
             $scope.VehicleManifest.Modified =  response.data[0].Modified;
             $scope.VehicleManifest.ArrivalDate =  response.data[0].ArrivalDate;
             $scope.VehicleManifest.ManifestUnloaded =  response.data[0].ManifestUnloaded;
             $scope.VehicleManifest.DeliveryStatus =  response.data[0].DeliveryStatus;
             
             $scope.ManifestSaved = true;
         }
     },
     function errorCallback(response) {
         alert(response);
     });
    }


    $scope.clearManifest = function () {
        $scope.showManifestConsignments = false;
        $scope.ManifestSaved = false;
        $scope.ManifestVehicleNo = '';
        $scope.ManifestVehicleType = '';
        $scope.VehicleManifest = {
            ManifestID: '',
            VehicleHireID: '',
            ManifestNo: '',
            DriverName: '',
            DriverMobileNo: '',
            LicenseNo: '',
            EstimatedDeliveryDate: '',
            ArrivalTime: '',
            DepartureTime: $scope.currentTime,
            FromLoc: '',
            ToLoc: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            ArrivalDate: '',
            ManifestUnloaded: '',
            DeliveryStatus: '',
        };
    }

    $scope.ManifestVehicleNo = '';
    $scope.ManifestVehicleType = '';
    $scope.closeManifest = function () {

        $scope.clearManifest();
        $('#ManifestModal').modal('hide');
    }
   
    $scope.AddManifest = function (vehicleHireItem) {
        $scope.clearManifest();
        $scope.GetBranches();
        $scope.loadVehicleManifest(vehicleHireItem);
        $scope.VehicleManifest.DepartureTime = $scope.currentTime;
       
        $scope.GetSelectedVehicleForHire(vehicleHireItem.VehicleID); 
        $scope.VehicleManifest.VehicleHireID = vehicleHireItem.VehicleHireID;
        $scope.VehicleManifest.Created = $scope.currentDate;
        $('#ManifestModal').modal('show');
    }
    $scope.GetSelectedVehicleForHire = function(vehicleID)
    {

        $http({
            method: 'GET',
            params: { 'vehicleID': vehicleID, },
            url: '/Operations/GetVehicleByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    .then(function successCallback(response) {
        if (response.data.length > 0) {
            $scope.ManifestVehicleNo = response.data[0].VehicleNo;
        }
    
    
    },
     function errorCallback(response) {
         alert(response);
     });
        
    }

    $scope.DeleteManifest = function (manifestID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteVehicleHireManifest',
                params: { 'manifestID': manifestID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
          .then(function successCallback(response) {
              Notification.success({ message: "Vehicle Manifest deleted successfully.", delay: 5000 });
              $scope.loadVehicleHirePayments();
          }, function errorCallback(response) {
              var e = response.message;
          });
        }
    }
    $scope.SaveManifest  = function () {

        if ($scope.frmManifest.$valid) {
            if ($scope.VehicleManifest.ManifestID== '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddVehicleHireManifest',
                    cache: false,
                    data: $scope.VehicleManifest,
                    datatype: 'json',
                    contentType: "application/json; charset=utf-8",
                })
             .then(function successCallback(response) {
                 console.log(response);
                 $scope.clearManifest();
                // $('#ManifestModal').modal('hide');
                 Notification.success({ message: "Vehicle Manifest saved successfully.", delay: 5000 });
                 var manifestID = response.data;

                 $scope.VehicleManifest.ManifestID = manifestID;

                 $scope.ManifestSaved = true;

             }, function errorCallback(response) {
                 Notification.error({ message: response.data, delay: 5000 });
             });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateVehicleHireManifest',
                    cache: false,
                    data : $scope.VehicleManifest ,
                    datatype: 'json',
                    contentType: "application/json; charset=utf-8",
                })
               .then(function successCallback(response) {
                   console.log(response);
                   $scope.clearManifest();
                   $('#ManifestModal').modal('hide');
                   $scope.ManifestSaved = true;
                   Notification.success({ message: "Vehicle Manifest updated successfully.", delay: 5000 });
                   //$scope.loadConsignmentBookings();

               }, function errorCallback(response) {
                   Notification.error({ message: response.data, delay: 5000 });
               });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    
    //Manifest Consignments
    $scope.ManifestConsignment = {
        ManifestConsignmentID: '',
        ManifestID: '',
        ConsignmentID: '',
        CreatedBy: '',
        Created: ''
    }
    $scope.ManifestConsignment = [];
    $scope.ShowConsignment = function () {
        $scope.GetConsignmentsForManifest();
        //$scope.GetManifestConsignments();
        $scope.showManifestConsignments = true;
    }
    $scope.CancelConsignment = function () {
        
        $scope.activeConsignmentBooking = [];
        $scope.showManifestConsignments = false;
    }

    $scope.GetManifestConsignments = function () {
        
        $http({
            method: 'GET',
            params: { 'manifestID': $scope.VehicleManifest.ManifestID },
            url: '/Operations/GetManifestConsignments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             //manifestCnsgnment.push(response.data[i]);

             for (j = 0; j < $scope.activeConsignmentBooking.length; j++) {
                 if ($scope.activeConsignmentBooking[j].ConsignmentID == response.data[i].ConsignmentID) {
                     $scope.activeConsignmentBooking[j].checkSelected = true;
                     break;
                 }
             }
         }
         $scope.ManifestConsignment = manifestCnsgnment;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.AddManifestConsignments = function () {

        var newManifestConsignments = [];
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            //var traingOpp  = [];
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {

                var mfConsignment = {
                    ManifestConsignmentID: '',
                    ManifestID: $scope.VehicleManifest.ManifestID,
                    ConsignmentID: $scope.activeConsignmentBooking[i].ConsignmentID,
                    CreatedBy: '',
                    Created: ''
                }
                newManifestConsignments.push(mfConsignment);
                // traingOpp.push(activetrainingPlanOpps[i]);
            }


        }

        $http({
            method: 'POST',
            url: '/Operations/AddManifestConsignment',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data:  JSON.stringify(newManifestConsignments) 
            
        })
         .then(function successCallback(response) {
             console.log(response);
          

             Notification.success({ message: "Manifest consignments saved successfully.", delay: 5000 });
           
         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
        //}
    }

    /*End Manifest*/
  
    /* END UNLOAD MANIFEST CONSIGNMENT */

}]);
App.controller('UnloadManifest_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {
  
    $scope.activeManifest = [];
    $scope.activeManifestCollection = [];
    $scope.VehicleUnloadManifest = {
        ManifestID: '',
        VehicleHireID: '',
        ManifestNo: '',
        DriverName: '',
        DriverMobileNo: '',
        LicenseNo: '',
        EstimatedDeliveryDate: '',
        ArrivalTime: '',
        DepartureTime: '',
        FromLoc: '',
        ToLoc: '',
        ArrivalDate:'',
        FromBranch: '',
        ToBranch: ''
    };
    $scope.GetManifestForUnload = function () {
        var manifest = [];
        $http({
            method: 'GET',
            url: '/Operations/GetManifestsForUnloadByToBranchID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             manifest.push(response.data[i]);
                 
            
             }
         
         $scope.activeManifest = manifest;
         $scope.activeManifestCollection = manifest;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }

    /* UNLOAD MANIFEST CONSIGNMENT */
    $scope.ReceivedStatus = [];
    $scope.GetConsignmentReceivedStatus = function () {

        //$scope.activetrainingOpportunities = '';
        var rcdStatus = [];
        $http({
            method: 'GET',
            url: '/Operations/GetConsignmentReceivedStatus',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             rcdStatus.push(response.data[i]);
         }
         $scope.ReceivedStatus = rcdStatus;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.ReceivedStatusOK = [];
    $scope.ManifestConsignmentsUnloaded = {
        ManifestConsignmentID: '',
        ManifestID: '',
        ConsignmentID: '',
        CreatedBy: '',
        Created: ''
    }
    $scope.ManifestConsignmentsUnloaded = [];
    $scope.UnloadManifestConsignment = [];

    $scope.CheckReceived = function (index) {
        if ($scope.UnloadManifestConsignment[index].ReceivedStatus == 1)//Status OK
        {
            $scope.ReceivedStatusOK[index] = true;
        }
        else {
            $scope.ReceivedStatusOK[index] = false;
        }
    }



    $scope.GetUnloadManifestConsignments = function (manifestID) {
        var mfConsign = [];
        $http({
            method: 'GET',
            params: { 'manifestID': manifestID },
            url: '/Operations/GetUnloadManifestConsignments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             mfConsign.push(response.data[i]);
         }
         $scope.UnloadManifestConsignment = mfConsign;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.currentTime = $filter('date')(new Date(), 'hh:mm:ss');
    $scope.ManifestVehicleNo = '';
    $scope.GetVehicleManifestUnload = function (manifestItem) {
        $scope.UnloadManifestConsignment = [];
        
        $scope.VehicleUnloadManifest.ManifestID =  manifestItem.ManifestID;
        $scope.VehicleUnloadManifest.VehicleHireID =  manifestItem.VehicleHireID;
        $scope.VehicleUnloadManifest.ManifestNo =  manifestItem.ManifestNo;
        $scope.VehicleUnloadManifest.DriverName =  manifestItem.DriverName;
        $scope.VehicleUnloadManifest.DriverMobileNo =  manifestItem.DriverMobileNo;
        $scope.VehicleUnloadManifest.LicenseNo =  manifestItem.LicenseNo;
        $scope.VehicleUnloadManifest.EstimatedDeliveryDate = $filter('date')(manifestItem.EstimatedDeliveryDate, 'dd/MM/yyyy');
        $scope.VehicleUnloadManifest.ArrivalTime =  $scope.currentTime;
        $scope.VehicleUnloadManifest.DepartureTime =  manifestItem.DepartureTime;
        $scope.VehicleUnloadManifest.FromLoc = manifestItem.FromLoc;
        $scope.VehicleUnloadManifest.ArrivalDate = $scope.currentDate;
        $scope.VehicleUnloadManifest.ToLoc = manifestItem.ToLoc;
        $scope.VehicleUnloadManifest.ToBranch = manifestItem.ToBranch;
        $scope.VehicleUnloadManifest.FromBranch = manifestItem.FromBranch;
        $scope.ManifestVehicleNo = manifestItem.VehicleNo;
        $scope.GetConsignmentReceivedStatus();
        $scope.GetUnloadManifestConsignments($scope.VehicleUnloadManifest.ManifestID);

        $('#UnloadManifestModal').modal('show');
       
    }
    $scope.CancelConsignment = function () {
        $scope.UnloadManifestConsignment = [];
        $scope.ManifestConsignmentsUnloaded = [];
        $scope.ManifestVehicleNo = '';
        $scope.VehicleUnloadManifest = '';
        $('#UnloadManifestModal').modal('hide');
    }
    $scope.UpdateManifestConsignmentsStatus = function () {

        for (i = 0; i < $scope.UnloadManifestConsignment.length; i++) {
            $scope.ManifestConsignmentsUnloaded.push({
                ManifestConsignmentID: $scope.UnloadManifestConsignment[i].ManifestConsignmentID,
                ManifestID: $scope.UnloadManifestConsignment[i].ManifestID,
                ConsignmentID: $scope.UnloadManifestConsignment[i].ConsignmentID,
                ReceivedStatus: $scope.UnloadManifestConsignment[i].ReceivedStatus,
                ReceivedArticles: $scope.UnloadManifestConsignment[i].ReceivedArticles,
                Remarks: $scope.UnloadManifestConsignment[i].Remarks,

            });
        }
        $http({
            method: 'POST',
            url: '/Operations/UpdateManifestConsignmentStatus',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify($scope.ManifestConsignmentsUnloaded)

        })
         .then(function successCallback(response) {
             console.log(response);


             Notification.success({ message: "Manifest unloaded successfully.", delay: 5000 });
             $scope.GetManifestForUnload();
             $('#UnloadManifestModal').modal('hide');
         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
        //}
    }


}]);

App.controller('ManifestDelivery_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {
    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }
    $('#txtDeliveryDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtDeliveryDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $scope.deliveryStatusTypes = [];
    $scope.GetDeliveryStatusMaster = function () {

        //$scope.activetrainingOpportunities = '';
        var statusTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetDeliveryStatusMaster',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             statusTypes.push(response.data[i]);
         }
         $scope.deliveryStatusTypes = statusTypes;
         // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.manifestConsignment = {
        ManifestConsignmentID: '',
        ManifestID: '',
        ConsignmentID: '',
        CreatedBy: '',
        Created: '',
        ReceivedStatus: '',
        Remarks: '',
        ReceivedArticles: '',
        unloadStatus: '',
        DeliveryStatus: '',
        DeliveryDate: '',
        DeliveryRemarks: '',
        DeliveredArticles: '',
    }

    $scope.showDeliveryModal = function (item) {

        $scope.GetDeliveryStatusMaster();
        
        $scope.manifestConsignment.DeliveryStatus = item.DeliveryStatus;
            $scope.manifestConsignment.DeliveryDate = item.DeliveryDate;
            $scope.manifestConsignment.DeliveredArticles = item.DeliveredArticles;
            $scope.manifestConsignment.DeliveryRemarks = item.DeliveryRemarks;
            $scope.manifestConsignment.ManifestConsignmentID = item.ManifestConsignmentID;
            $scope.manifestConsignment.ManifestID = item.ManifestID;
            $scope.manifestConsignment.ConsignmentID = item.ConsignmentID;
        $('#DeliveryModal').modal('show');
    }
    $scope.closeModal = function () {
        $scope.manifestConsignment = {
            ManifestConsignmentID: '',
            ManifestID: '',
            ConsignmentID: '',
            CreatedBy: '',
            Created: '',
            ReceivedStatus: '',
            Remarks: '',
            ReceivedArticles: '',
            unloadStatus: '',
            DeliveryStatus: '',
            DeliveryDate: '',
            DeliveryRemarks: '',
            DeliveredArticles: '',
        }
        $('#DeliveryModal').modal('hide');
    }
    
    $scope.activeConsignments = [];
    $scope.activeConsignmentCollection = [];
    $scope.GetConsignmentsForDelivery = function () {
        var consignment = [];
        $http({
            method: 'GET',
            url: '/Operations/GetManifestConsignmentsForDelivery',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             consignment.push(response.data[i]);


         }

         $scope.activeConsignments = consignment;
         $scope.activeConsignmentCollection = consignment;
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.UpdateManifestConsignmentsStatus = function () {

        
        $http({
            method: 'POST',
            url: '/Operations/UpdateManifestConsignmentDeliveryStatus',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify($scope.manifestConsignment)

        })
         .then(function successCallback(response) {
             console.log(response);


             Notification.success({ message: "Delivery Status updated successfully.", delay: 5000 });
             $scope.GetConsignmentsForDelivery();
             $scope.closeModal();
         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
        //}
    }


}]);
App.controller('ManifestReport_ngController', ["$scope", "$sce","$http", "$filter", "filterFilter", "Notification", function ($scope,$sce, $http, $filter, filterFilter, Notification) {
    $scope.Branches = [];
    $scope.FromLoc = '';
    $scope.ToLoc = '';
    $scope.CreatedDate = '';
    $('#txtCreatedDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtCreatedDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             brnches.push(response.data[i]);
         }
         $scope.Branches = brnches;
     
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.InitReport = function()
    {
        $scope.FromLoc = '';
        $scope.ToLoc = '';
        $scope.CreatedDate = '';
        $scope.GetBranches();
    }
    $scope.activeManifests = [];
    $scope.activeManifestCollection = [];
    $scope.GetManifests = function () {
        var manifest = [];
        if ($scope.FromLoc == "")
            $scope.FromLoc = 0;
        if ($scope.ToLoc == "")
            $scope.ToLoc = 0;
        $http({
            method: 'GET',
            params: { 'FromId': $scope.FromLoc, 'ToId': $scope.ToLoc, 'createdDate': $scope.CreatedDate },
            url: '/Operations/GetManifestsForReport',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             manifest.push(response.data[i]);
         }

         $scope.activeManifests = manifest;
         $scope.activeManifestCollection = manifest;
     },
     function errorCallback(response) {
         alert(response);
     });
    }
    $scope.ReportURL = '';
    $scope.ViewReports = function (manifestId) {
        $scope.ReportURL = $sce.trustAsResourceUrl("../Common/ReportViewer.aspx?manifestID=" + manifestId );
        $('#ViewReportModal').modal("show");
    }
    $scope.closeReport = function () {
        $scope.ReportURL ='';
        $('#ViewReportModal').modal("hide");
    }
}]);