//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingPaymentTax
    {
        public int PaymentTaxesID { get; set; }
        public int BookingPaymentID { get; set; }
        public string TaxType { get; set; }
        public double TaxValue { get; set; }
    }
}
